package com.kinon.ceshiaccount.fragment.home;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.application.AccountApplication;
import com.kinon.ceshiaccount.bean.base.BaseBean;
import com.kinon.ceshiaccount.fragment.BaseFragment;
import com.kinon.ceshiaccount.network.HttpManage;
import com.kinon.ceshiaccount.network.MyCallback;
import com.kinon.ceshiaccount.util.SPManage;
import com.kinon.ceshiaccount.view.computer.ComputerNumView;

import java.lang.reflect.Method;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by luohao on 2017-10-13.
 */

public class TiedNewCardFragment extends BaseFragment implements View.OnClickListener,
        ComputerNumView.OnComputerListener, View.OnFocusChangeListener {
    private View mView;
    private Context mContext;
    private EditText et_tCard;
    private EditText et_tiedphone;
    private TextView tv_getCode;
    private EditText et_tiedCardCode;
    private Button btn_tiedCard;
    private ComputerNumView cview_numCard;

    private String mCode = "";
    private String mPhone = "";

    private String mStrPhone = "";
    private String mStrCode = "";

    private int mEtType = 0;

    private int mTime = 60;
    private Handler timeHandler = new Handler();
    private Runnable timeRunnable = new Runnable() {
        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTime--;
                    if (mTime <= 0) {
                        tv_getCode.setText("获取验证码");
                        tv_getCode.setEnabled(true);
                    } else {
                        tv_getCode.setText("已发送(" + mTime + "s)");
                        timeHandler.postDelayed(timeRunnable, 1000);
                    }

                }
            });
        }
    };

    public TiedNewCardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_tiedcard_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = AccountApplication.getInstance();
        initView();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {

            clear();
        }
    }

    private void initView() {
        et_tCard = (EditText) mView.findViewById(R.id.et_tCard);
        et_tiedphone = (EditText) mView.findViewById(R.id.et_tiedphone);
        tv_getCode = (TextView) mView.findViewById(R.id.tv_getCode);
        et_tiedCardCode = (EditText) mView.findViewById(R.id.et_tiedCardCode);
        btn_tiedCard = (Button) mView.findViewById(R.id.btn_tiedCard);
        cview_numCard = (ComputerNumView) mView.findViewById(R.id.cview_numCard);

        btn_tiedCard.requestFocus();
//        et_tiedCardCode.setEnabled(true);
        tv_getCode.setOnClickListener(this);
        btn_tiedCard.setOnClickListener(this);
        cview_numCard.setOnComputerListener(this);

        hidekey(et_tCard);
        hidekey(et_tiedphone);
        hidekey(et_tiedCardCode);
        et_tCard.setOnFocusChangeListener(this);
        et_tiedphone.setOnFocusChangeListener(this);
        et_tiedCardCode.setOnFocusChangeListener(this);

    }

    //不弹出键盘
    private void hidekey(EditText et) {
        getActivity().getWindow().setSoftInputMode
                (WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        try {
            Class<EditText> cls = EditText.class;
            Method setSoftInputShownOnFocus;
            setSoftInputShownOnFocus = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
            setSoftInputShownOnFocus.setAccessible(true);
            setSoftInputShownOnFocus.invoke(et, false);
        } catch (Exception e) {

        }

    }

    private void clear() {
        et_tCard.setText("");
        et_tiedphone.setText("");
        et_tiedCardCode.setText("");
        mCode = "";
        mPhone = "";

        mStrPhone = "";
        mStrCode = "";
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_getCode:

//                mTimer.schedule(mTask,mTime,1000);
                getCode();
                break;
            case R.id.btn_tiedCard:
                tiedCard();
                break;
        }
    }

    private void getCode() {
        et_tiedCardCode.setText("");
        mCode = "";
        mPhone = et_tiedphone.getText().toString().trim();

        if (TextUtils.isEmpty(mPhone)) {
            Toast.makeText(mContext, R.string.hint_inputphone,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        mTime = 60;
        tv_getCode.setEnabled(false);
        timeHandler.postDelayed(timeRunnable, 1000);
        Call<BaseBean<String>> call = HttpManage.getRequestApi().
                sendCodeV2(SPManage.getInstance().getToken(), mPhone);
        call.enqueue(new MyCallback<BaseBean<String>>() {
            @Override
            public void onSuccess(Call<BaseBean<String>> call,
                                  Response<BaseBean<String>> response) {
                mCode = response.body().getData();
                Toast.makeText(mContext, response.body().getMsg() + "",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(int code, String error) {
//                 Toast.makeText(mContext,error,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void tiedCard() {
        String card = et_tCard.getText().toString().trim();
        String code = et_tiedCardCode.getText().toString().trim();
        if (TextUtils.isEmpty(card)) {
            Toast.makeText(mContext, R.string.hint_clickAndCreditCard,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(code)) {
            Toast.makeText(mContext, R.string.hint_inputcode,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (!mCode.equals(code)) {
            Toast.makeText(mContext, R.string.code_error,
                    Toast.LENGTH_SHORT).show();
            return;
        }


        Call<BaseBean<String>> call = HttpManage.getReqLunchApi().
                bindCardV2(SPManage.getInstance().getToken(), mPhone, card, code);
        call.enqueue(new MyCallback<BaseBean<String>>() {
            @Override
            public void onSuccess(Call<BaseBean<String>> call, Response<BaseBean<String>> response) {
                clear();
                Toast.makeText(mContext, response.body().getMsg() + "",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(int code, String error) {

                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onComputerListener(String num) {
        //p代表.
        if (num.equals("p")) {
            //小数点
            return;
        }
        //j代表加
        else if (num.equals("j")) {
            //加号计算
            return;
        }
        //c代表清除
        else if (num.equals("c")) {
            //清除计算
            switch (mEtType) {
                case 0:
                    et_tCard.setText("");
                    break;
                case 1:
                    mStrPhone = "";
                    et_tiedphone.setText(mStrPhone);
                    break;

                case 2:
                    mStrCode = "";
                    et_tiedCardCode.setText(mStrCode);
                    break;
            }
        }
        //o代表完成
        else if (num.equals("o")) {

        }
        //d代表删除上一个
        else if (num.equals("d")) {
            //删除上一个

            switch (mEtType) {
                case 0:

                    break;

                case 1:
                    if (mStrPhone.length() > 0) {
                        mStrPhone = mStrPhone.substring(0, mStrPhone.length() - 1);
                        et_tiedphone.setText(mStrPhone);
                    }
                    break;

                case 2:
                    if (mStrCode.length() > 0) {
                        mStrCode = mStrCode.substring(0, mStrCode.length() - 1);
                        et_tiedCardCode.setText(mStrCode);
                    }
                    break;
            }

        }
        //除了上面几种特殊键就是数字键
        else {
            switch (mEtType) {
                case 0:
                    break;
                case 1:
                    mStrPhone += num;
                    et_tiedphone.setText(mStrPhone);
                    break;

                case 2:
                    mStrCode += num;
                    et_tiedCardCode.setText(mStrCode);
                    break;
            }

        }

    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.et_tCard:
                if (b) {
                    mEtType = 0;
                }
                break;
            case R.id.et_tiedphone:
                if (b) {
                    mEtType = 1;
                }
                break;
            case R.id.et_tiedCardCode:
                if (b) {
                    mEtType = 2;
                }
                break;
        }
    }
}
