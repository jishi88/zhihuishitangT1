package com.kinon.ceshiaccount.wxpay;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by luohao on 2017-09-22.
 */

public class WxPay {


    //终端IP
    static String spbill_create_ip = "192.168.199.193";
    //随机字符串
    String nonce_str;
    //        商品描述
    String body = "";
    //商户订单号
    String out_trade_no;
    //交易金额
    int total_fee = 0;
    //授权码(二维码)
    String auth_code;
    private String sign;

    public static Map<String, String> addMapInfo(String body, int total_fee, String auth_code) {

        Map<String, String> params = new HashMap<String, String>();
        String out_trade_no = WXPayUtil.generateNonceStr();

        params.put("appid", WXPayConstants.APP_ID);
        params.put("mch_id", WXPayConstants.MCH_ID);
        params.put("device_info", WXPayConstants.DEVICE_INFO);
        params.put("body", body);
        params.put("nonce_str", WXPayUtil.generateNonceStr());
        Log.e("lh", "nonce_str==" + WXPayUtil.generateNonceStr());
        params.put("out_trade_no", out_trade_no);
        Log.e("lh", "out_trade_no==" + out_trade_no);
        params.put("total_fee", total_fee + "");
        params.put("spbill_create_ip", spbill_create_ip);
        params.put("auth_code", auth_code);
        Log.e("lh", "auth_code==" + auth_code);
        try {
            String sign = WXPayUtil.generateSignature(params, WXPayConstants.WX_KEY);

            params.put("sign", sign);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return params;
    }

    public static String request(String body, int total_fee, String auth_code) {
        String response = "";
        try {
            String content = WXPayUtil.mapToXml(addMapInfo(body, total_fee, auth_code));
            response = postData("https://api.mch.weixin.qq.com/pay/orderquery", content);
            Log.e("lh", "response==" + response);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }


    public static String postData(String urlStr, String data) {
        return postData(urlStr, data, "text/xml");
    }

    public static String postData(String urlStr, String data, String contentType) {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlStr);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            if (contentType != null)
                conn.setRequestProperty("Content-Type", "text/xml");
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            if (data == null)
                data = "";
            writer.write(data);
            writer.flush();
            writer.close();

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append("\r\n");
            }
            return sb.toString();
        } catch (IOException e) {
            Log.e("lh", "Error connecting to " + urlStr + ": " + e.getMessage());
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
            }
        }
        return null;
    }

}
