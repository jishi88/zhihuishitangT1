package com.kinon.ceshiaccount.util;


import android.content.Context;
import android.os.Handler;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.view.LoadingDialog;

public class MyShowDialog {
    // 加载使用的dialog
    public static LoadingDialog loadingDialog;

    public static void showLoadingDialog(Context context, String paramString) {
        loadingDialog = new LoadingDialog(context, R.style.ql_loading_dialog,
                R.layout.custom_loading_layout);
        loadingDialog.setCanceledOnTouchOutside(true);
        loadingDialog.setCancelable(true);
        loadingDialog.show();
        loadingDialog.setText(paramString);
    }

    /**
     * 关闭加载的图标
     */
    public static void closeLoadingDialog() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (loadingDialog != null) {
                        loadingDialog.dismiss();
                        loadingDialog = null;
                    }
                } catch (Exception e) {

                }
            }
        }, 100);
    }
}
