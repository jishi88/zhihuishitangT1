package com.kinon.ceshiaccount.adapter.urgent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.bean.urgent.U_SettletmentBean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-07-21.
 * 午餐的适配器
 */

public class U_LunchAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<U_SettletmentBean> list;

    public U_LunchAdapter(Context context, ArrayList<U_SettletmentBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHandle handle;
        if (view == null) {
            handle = new ViewHandle();
            view = LayoutInflater.from(context).inflate(R.layout.item_lunch_layout, null);
            handle.tv_diners = (TextView) view.findViewById(R.id.tv_diners);
            handle.tv_diningNum = (TextView) view.findViewById(R.id.tv_diningNum);
            handle.tv_diningMoney = (TextView) view.findViewById(R.id.tv_diningMoney);
            view.setTag(handle);
        } else {
            handle = (ViewHandle) view.getTag();
        }
        handle.tv_diners.setText(list.get(i).getCard());
        handle.tv_diningNum.setText(list.get(i).getNum() + "");
        handle.tv_diningMoney.setText(list.get(i).getTotal());
        return view;
    }

    private class ViewHandle {
        /**
         * 就餐人姓名
         */
        private TextView tv_diners;
        /**
         * 就餐次数
         */
        private TextView tv_diningNum;
        /**
         * 就餐的金额
         */
        private TextView tv_diningMoney;

    }
}
