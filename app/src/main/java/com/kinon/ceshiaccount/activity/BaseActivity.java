package com.kinon.ceshiaccount.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.kinon.ceshiaccount.constant.Constant;

/**
 * Created by luohao on 2017-06-15.
 * activity的公共基础类
 */

public class BaseActivity extends AppCompatActivity {

    protected String TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = "lh-" + getComponentName().getShortClassName() + "==";
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//        noBar();
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);//remove notification bar  即全屏
    }


    private void noBar() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        int uiFlags = View.SYSTEM_UI_FLAG_FULLSCREEN    //全屏会显示导航栏
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION  //隐藏导航栏
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION  //隐藏导航栏布局
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        uiFlags |= 0x00001000;
        getWindow().getDecorView().setSystemUiVisibility(uiFlags);
    }

    protected String getToken() {
        SharedPreferences sp = getSharedPreferences("a_user", Context.MODE_PRIVATE);
        return sp.getString("token", "");
    }


    /**
     * 申请指定权限
     *
     * @param code
     * @param permission
     */
    public void requestPermission(int code, String... permission) {
        ActivityCompat.requestPermissions(this, permission, code);
    }

    /**
     * 判断是否有指定权限
     */
    public boolean hasPermission(String strPermission) {
        int permission = ActivityCompat.checkSelfPermission(this,
                strPermission);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    /**
     * 判断是否有指定的权限
     */
    public boolean hasPermission(String... permissions) {
        for (String permisson : permissions) {
            if (ContextCompat.checkSelfPermission(this, permisson)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.READ_PHONE_STATE_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    doSDCardPermission();

                    //了权限同意
//                    assentPermission(requestCode);
                } else {
                    //拒绝权限
                    //判断版本是否是需求版本
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        // 判断用户是否 点击了不再提醒。(检测该权限是否还可以申请)
                        boolean b = shouldShowRequestPermissionRationale(permissions[0]);
                        if (!b) {
                            //还想开启权限
                            onReadPhoneStatePermission();
                        } else {
                            finish();
                        }
                    }
                }
                break;
//            case Constant.WRITE_EXTERNAL_STORAGE:
//                assentPermission(requestCode);
//                break;
            default:
                break;
        }
    }

    protected void assentPermission(int requestCode) {

    }

    /**
     * 申请权限是点击了否，进行处理的方法
     */
    protected void onReadPhoneStatePermission() {

    }

    public void Mtoast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
