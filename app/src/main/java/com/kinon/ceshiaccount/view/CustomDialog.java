package com.kinon.ceshiaccount.view;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.adapter.DiningTypeAdapter;
import com.kinon.ceshiaccount.adapter.PayTypeAdapter;
import com.kinon.ceshiaccount.bean.RoleBean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-07-18.
 * 自定义的dialog
 */

public class CustomDialog extends Dialog implements View.OnClickListener {


    private ViewGroup mView;
    private LinearLayout ll_dialogTop;
    private TextView tv_dialogTitle;
    private TextView tv_dialogMessage;
    private TextView tv_dialogNo;
    private TextView tv_dialogYes;
    private GridView grid_dialogType;
    private LinearLayout ll_confrim;
    private LinearLayout ll_dialogP;
    private EditText et_diaPhone;


    private DialogClickListener listener;
    private PayTypeAdapter diningAdapter = null;
    private DiningTypeAdapter typeAdapter = null;
    private String mStrMsg = "";
    private int mType;
//    private DialogItemClickListener itemListener;
    private AdapterView.OnItemClickListener onItemclick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//            Log.e("lh","i=="+i);
            diningAdapter.selectItem(i);
            diningAdapter.notifyDataSetChanged();
            listener.onDialogClick(i);
            dismiss();
        }
    };

    public CustomDialog(Context context, String msg,
                        DialogClickListener confirmListener) {
        this(0, context, null, msg, null, null, 0, null, 0, confirmListener);
    }

    public CustomDialog(int type, Context context, /* String msg,*/
                        DialogClickListener confirmListener) {
        this(type, context, null, null, null, null, 0, null, 0, confirmListener);
    }

    public CustomDialog(Context context, String title, String msg,
                        String confirm, String cancel, DialogClickListener confirmListener) {

        this(0, context, title, msg, confirm, cancel, 0, null, 0, confirmListener);
    }

    public CustomDialog(int dType, Context context, String title, ArrayList<RoleBean> binings,
                        int index, DialogClickListener confirmListener) {

        this(dType, context, title, null, null, null, 0, binings, index, confirmListener);
    }

    public CustomDialog(int dType, Context context, String title, String msg, String confirm, String cancel,
                        int msgSize, ArrayList<RoleBean> binings, int index,
                        DialogClickListener listener) {
        super(context, R.style.Base_Theme_AppCompat_Dialog);
        this.listener = listener;
        this.mStrMsg = msg;
        mType = dType;
        initDialog(dType, context, title, msg, confirm, cancel, binings, index);
    }

    private void initDialog(int dType, Context context, String title, String message,
                            String confirm, String cancel, ArrayList<RoleBean> binings, int index) {
        setContentView(createDialogView(context, R.layout.dialog_context_layout));
        setParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        ll_dialogTop = (LinearLayout) mView.findViewById(R.id.ll_dialogTop);
        tv_dialogTitle = (TextView) mView.findViewById(R.id.tv_dialogTitle);
        tv_dialogMessage = (TextView) mView.findViewById(R.id.tv_dialogMessage);
        tv_dialogNo = (TextView) mView.findViewById(R.id.tv_dialogNo);
        tv_dialogYes = (TextView) mView.findViewById(R.id.tv_dialogYes);
        grid_dialogType = (GridView) mView.findViewById(R.id.grid_dialogType);
        ll_confrim = (LinearLayout) mView.findViewById(R.id.ll_confrim);
        ll_dialogP = (LinearLayout) mView.findViewById(R.id.ll_dialogP);
        tv_dialogNo.setOnClickListener(this);
        tv_dialogYes.setOnClickListener(this);

        if (dType == 1) {
            ll_dialogP.setVisibility(View.GONE);
            grid_dialogType.setVisibility(View.VISIBLE);

            int numColumns = getNumColumn(binings.size());
            grid_dialogType.setNumColumns(numColumns);
            int wigth = 240 * numColumns +
                    80 * (numColumns + 1);
            //单选
//             grid_dialogType.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
            setParams(wigth, WindowManager.LayoutParams.WRAP_CONTENT);
            diningAdapter = new PayTypeAdapter(context, binings, index);
            grid_dialogType.setAdapter(diningAdapter);
            grid_dialogType.setOnItemClickListener(onItemclick);

        } else if (dType == 2) {
//             setParams(400, WindowManager.LayoutParams.WRAP_CONTENT);
            et_diaPhone = (EditText) mView.findViewById(R.id.et_diaPhone);
            ll_dialogP.setVisibility(View.VISIBLE);
            grid_dialogType.setVisibility(View.GONE);
            et_diaPhone.setVisibility(View.VISIBLE);

        }
        //判断标题是否为空
        if (TextUtils.isEmpty(title)) {
            ll_dialogTop.setVisibility(View.GONE);
        } else {
            tv_dialogTitle.setText(title);
        }
        if (!TextUtils.isEmpty(message)) {
            tv_dialogMessage.setText(message);
        }
        if (!TextUtils.isEmpty(cancel)) {
            tv_dialogNo.setText(cancel);
        }
        if (!TextUtils.isEmpty(confirm)) {
            tv_dialogYes.setText(confirm);
        }

    }

    public String getmStrMsg() {
        return mStrMsg;
    }

    public void setmStrMsg(String mStrMsg) {
        this.mStrMsg = mStrMsg;
        tv_dialogMessage.setVisibility(View.VISIBLE);
        et_diaPhone.setVisibility(View.GONE);
        tv_dialogMessage.setTextSize(25);
        tv_dialogMessage.setPadding(30, 30, 30, 30);
        tv_dialogMessage.setGravity(Gravity.LEFT);
        tv_dialogMessage.setText(mStrMsg);
        tv_dialogNo.setText("取消支付");
        tv_dialogYes.setText("确认支付");
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public String getPhone() {
        return et_diaPhone.getText().toString().trim();
    }

    @Override
    public void onClick(View view) {
        if (listener == null) {
            dismiss();
            Log.e("", "listener==null");
            return;
        }
        switch (view.getId()) {
            case R.id.tv_dialogNo:
                listener.onDialogClick(0);
                dismiss();

                break;
            case R.id.tv_dialogYes:
                listener.onDialogClick(1);
                if (mType == 2) {
                    return;
                }
                dismiss();
                break;
        }
    }

    public void setParams(int width, int height) {
        WindowManager.LayoutParams dialogParams = this.getWindow().getAttributes();
        dialogParams.width = width;
        dialogParams.height = height;
        this.getWindow().setAttributes(dialogParams);
    }

    private int getNumColumn(int typeNum) {
        int numColumns = 2;
        if (typeNum <= 4) {
        } else if (typeNum > 4 && typeNum <= 6) {
            numColumns = 3;
        } else {
            numColumns = 4;
        }
        return numColumns;
    }

    /**
     * 找到布局
     *
     * @param layoutId 布局id
     * @return
     */
    private ViewGroup createDialogView(Context context, int layoutId) {
        mView = (ViewGroup) LayoutInflater.from(context).inflate(layoutId, null);
        return mView;
    }

    public View findChildViewById(int id) {
        return mView.findViewById(id);
    }

    public void setDialogClickListener(DialogClickListener listener) {
        this.listener = listener;
    }

    public interface DialogClickListener {
        public void onDialogClick(int btn);
    }


}
