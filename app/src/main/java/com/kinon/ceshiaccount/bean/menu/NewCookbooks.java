package com.kinon.ceshiaccount.bean.menu;

import com.kinon.ceshiaccount.bean.CookbooksBean;

import java.util.ArrayList;

/**
 * Created by lhqq on 2018-02-06.
 */

public class NewCookbooks {
    private ArrayList<Breakfast> products;
    private ArrayList<CookbooksBean.Category> category;
    private int type;

    public ArrayList<Breakfast> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Breakfast> products) {
        this.products = products;
    }

    public ArrayList<CookbooksBean.Category> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<CookbooksBean.Category> category) {
        this.category = category;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
