package com.kinon.ceshiaccount.view.computer;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.kinon.ceshiaccount.R;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-06-20.
 * 自定义的计算机
 */

public class ComputerNumView extends RelativeLayout implements View.OnClickListener {
    public OnComputerListener listener;
    /**
     * 申明控件
     */
    private Button bt_computer0;
    private Button bt_computer1;
    private Button bt_computer2;
    private Button bt_computer3;
    private Button bt_computer4;
    private Button bt_computer5;
    private Button bt_computer6;
    private Button bt_computer7;
    private Button bt_computer8;
    private Button bt_computer9;
    private Button bt_computerPoint;
    private Button bt_computerPuls;
    private Button bt_computer_c;
    private Button bt_comComplete;
    private Button bt_computer_del;
    /**
     * 控件属性
     */
    private float textSize;
    private int textColor;
    private ArrayList<String> textList;

    public ComputerNumView(Context context) {
        super(context);
    }

    public ComputerNumView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initRes(context, attrs);
    }

    public void setOnComputerListener(OnComputerListener listener) {
        this.listener = listener;

    }

    private void initRes(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ComputerNumView);
//        textSize=ta.getInteger(R.styleable.ComputerNumView_computerTextSize,12);
        textSize = ta.getDimension(R.styleable.ComputerNumView_computerTextSize, 20);
        textColor = ta.getColor(R.styleable.ComputerNumView_computerTextColor, 0xffffff);
//        textColor=ta.getResourceId(R.styleable.ComputerNumView_computerTextColor,R.color.white);
        //避免浪费资源
        ta.recycle();

        initView(context, attrs);

    }

    private void initView(Context context, AttributeSet attrs) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.custom_computer_layout, this, true);

        bt_computer0 = (Button) findViewById(R.id.bt_computer0);
        bt_computer1 = (Button) findViewById(R.id.bt_computer1);
        bt_computer2 = (Button) findViewById(R.id.bt_computer2);
        bt_computer3 = (Button) findViewById(R.id.bt_computer3);
        bt_computer4 = (Button) findViewById(R.id.bt_computer4);
        bt_computer5 = (Button) findViewById(R.id.bt_computer5);
        bt_computer6 = (Button) findViewById(R.id.bt_computer6);
        bt_computer7 = (Button) findViewById(R.id.bt_computer7);
        bt_computer8 = (Button) findViewById(R.id.bt_computer8);
        bt_computer9 = (Button) findViewById(R.id.bt_computer9);
        bt_computerPoint = (Button) findViewById(R.id.bt_computerPoint);
        bt_computerPuls = (Button) findViewById(R.id.bt_computerPuls);
        bt_computer_c = (Button) findViewById(R.id.bt_computer_c);
        bt_comComplete = (Button) findViewById(R.id.bt_comComplete);
        bt_computer_del = (Button) findViewById(R.id.bt_computer_del);

        setAllTextColor();
        setAllTextSize();
        addOnclick();

//        bt_computer0=
    }

    /**
     * 设置所有字体颜色
     */

    private void setAllTextColor() {
        bt_computer0.setTextColor(textColor);
        bt_computer1.setTextColor(textColor);
        bt_computer2.setTextColor(textColor);
        bt_computer3.setTextColor(textColor);
        bt_computer4.setTextColor(textColor);
        bt_computer5.setTextColor(textColor);
        bt_computer6.setTextColor(textColor);
        bt_computer7.setTextColor(textColor);
        bt_computer8.setTextColor(textColor);
        bt_computer9.setTextColor(textColor);
        bt_computerPoint.setTextColor(textColor);
        bt_computerPuls.setTextColor(textColor);
        bt_computer_c.setTextColor(textColor);
        bt_comComplete.setTextColor(textColor);
        bt_computer_del.setTextColor(textColor);
    }

    /**
     * 设置所有的字体大小
     */
    private void setAllTextSize() {
        bt_computer0.setTextSize(textSize);
        bt_computer1.setTextSize(textSize);
        bt_computer2.setTextSize(textSize);
        bt_computer3.setTextSize(textSize);
        bt_computer4.setTextSize(textSize);
        bt_computer5.setTextSize(textSize);
        bt_computer6.setTextSize(textSize);
        bt_computer7.setTextSize(textSize);
        bt_computer8.setTextSize(textSize);
        bt_computer9.setTextSize(textSize);
        bt_computerPoint.setTextSize(textSize);
        bt_computerPuls.setTextSize(textSize);
        bt_computer_c.setTextSize(textSize);
        bt_comComplete.setTextSize(textSize);
        bt_computer_del.setTextSize(textSize);
    }

    /**
     * 綁定事件监听
     */
    private void addOnclick() {
        bt_computer0.setOnClickListener(this);
        bt_computer1.setOnClickListener(this);
        bt_computer2.setOnClickListener(this);
        bt_computer3.setOnClickListener(this);
        bt_computer4.setOnClickListener(this);
        bt_computer5.setOnClickListener(this);
        bt_computer6.setOnClickListener(this);
        bt_computer7.setOnClickListener(this);
        bt_computer8.setOnClickListener(this);
        bt_computer9.setOnClickListener(this);
        bt_computerPoint.setOnClickListener(this);
        bt_computerPuls.setOnClickListener(this);
        bt_computer_c.setOnClickListener(this);
        bt_comComplete.setOnClickListener(this);
        bt_computer_del.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (listener == null) {
            return;
        }
        switch (view.getId()) {

            case R.id.bt_computer0:
                listener.onComputerListener(bt_computer0.getText().toString());
                break;
            case R.id.bt_computer1:
                listener.onComputerListener(bt_computer1.getText().toString());
                break;
            case R.id.bt_computer2:
                listener.onComputerListener(bt_computer2.getText().toString());
                break;
            case R.id.bt_computer3:
                listener.onComputerListener(bt_computer3.getText().toString());
                break;
            case R.id.bt_computer4:
                listener.onComputerListener(bt_computer4.getText().toString());
                break;
            case R.id.bt_computer5:
                listener.onComputerListener(bt_computer5.getText().toString());
                break;
            case R.id.bt_computer6:
                listener.onComputerListener(bt_computer6.getText().toString());
                break;
            case R.id.bt_computer7:
                listener.onComputerListener(bt_computer7.getText().toString());
                break;
            case R.id.bt_computer8:
                listener.onComputerListener(bt_computer8.getText().toString());
                break;
            case R.id.bt_computer9:
                listener.onComputerListener(bt_computer9.getText().toString());
                break;
            case R.id.bt_computerPoint:
                listener.onComputerListener("p");
                break;
            case R.id.bt_computerPuls:
                listener.onComputerListener("j");
                break;
            case R.id.bt_computer_c:
                listener.onComputerListener("c");
                break;
            case R.id.bt_comComplete:
                listener.onComputerListener("o");
                break;
            case R.id.bt_computer_del:
                listener.onComputerListener("d");
                break;
                default:
                    break;

        }
    }

    public enum Computer {
        COMPUTER_NUM_0, COMPUTER_NUM_1, COMPUTER_NUM_2, COMPUTER_NUM_3, COMPUTER_NUM_4,
        COMPUTER_NUM_5, COMPUTER_NUM_6, COMPUTER_NUM_7, COMPUTER_NUM_8, COMPUTER_NUM_9,
        COMPUTER_NUM_POINT, COMPUTER_NUM_PULS, COMPUTER_NUM_C, COMPUTER_NUM_COMPLETE,
        COMPUTER_NUM_DEL

    }

    public interface OnComputerListener {
        void onComputerListener(String strNum);
    }
}
