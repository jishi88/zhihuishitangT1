package com.kinon.ceshiaccount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.application.AccountApplication;
import com.kinon.ceshiaccount.bean.DetailsBean;
import com.kinon.ceshiaccount.bean.Unprintnum;
import com.kinon.ceshiaccount.bean.base.BaseBean;
import com.kinon.ceshiaccount.bean.base.OrdersBean;
import com.kinon.ceshiaccount.network.HttpManage;
import com.kinon.ceshiaccount.network.MyCallback;
import com.kinon.ceshiaccount.network.StrCallback;
import com.kinon.ceshiaccount.sunmi.bean.TableBean;
import com.kinon.ceshiaccount.sunmi.print.AidlUtil;
import com.kinon.ceshiaccount.util.SPManage;
import com.kinon.ceshiaccount.view.CustomDialog;

import java.util.ArrayList;
import java.util.LinkedList;

import retrofit2.Call;
import retrofit2.Response;

import static com.kinon.ceshiaccount.R.string.print;

/**
 * Created by luohao on 2017-07-19.
 * 记录中的订单适配器
 */

public class OrderAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<OrdersBean> list;

    private OnPrintClickListener listener = null;

    public OrderAdapter(Context context, ArrayList<OrdersBean> list) {
        this.context = context;
        this.list = list;
    }

    public void setprintClickListener(OnPrintClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
//        Log.e("lh","getView---");
        final ViewHandler handler;
        if (view == null) {
            handler = new ViewHandler();
            view = LayoutInflater.from(context).inflate(R.layout.item_order_layout, null);
            handler.tv_takemealId = (TextView) view.findViewById(R.id.tv_takemealId);
            handler.tv_takemealName = (TextView) view.findViewById(R.id.tv_takemealName);
            handler.tv_takemealMoney = (TextView) view.findViewById(R.id.tv_takemealMoney);
            handler.tv_orderTime = (TextView) view.findViewById(R.id.tv_orderTime);
            handler.tv_takemealPrint = (TextView) view.findViewById(R.id.tv_takemealPrint);
            handler.tv_takemealNotice = (TextView) view.findViewById(R.id.tv_takemealNotice);
            view.setTag(handler);
        } else {
            handler = (ViewHandler) view.getTag();
        }
        handler.tv_takemealId.setText(list.get(i).getDayno());
        handler.tv_takemealName.setText(list.get(i).getName());
        handler.tv_takemealMoney.setText("¥" + list.get(i).getTotal());
        handler.tv_orderTime.setText(list.get(i).getOrder_time());

        //打印
        if (list.get(i).getPrint() == 1) {
            handler.tv_takemealPrint.setText(R.string.printed);
        } else {
            handler.tv_takemealPrint.setText(print);
        }
        //通知
        if (list.get(i).getNotice() == 1) {
            handler.tv_takemealNotice.setText(R.string.noticed);
        } else {
            handler.tv_takemealNotice.setText(R.string.notice);
        }
        //打印的监听事件
        handler.tv_takemealPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.get(i).getPrint() == 1) {
                    printDialog(handler.tv_takemealPrint, i, "是否重复打印");
                } else {
                    updataprint(i);

                }

            }
        });
        //通知的监听事件
        handler.tv_takemealNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.get(i).getNotice() != 1) {
                    notice(handler.tv_takemealNotice, i);
                }

            }
        });


        return view;
    }

    /**
     * 提示是否重复打印
     */
    private void printDialog(final TextView tv_orderRefunds, final int position, String msg) {
        CustomDialog dialog = new CustomDialog(context, null, msg, null, null,
                new CustomDialog.DialogClickListener() {
                    @Override
                    public void onDialogClick(int btn) {
                        if (btn == 1) {
                            print(position);
//                    refund(tv_orderRefunds,position);
                        }
                    }
                });
        dialog.show();
    }

    /**
     * 请求打印接口
     */
    private void updataprint(final int position) {
        Call<String> printcall = HttpManage.getRequestApi()
                .printOrder(SPManage.getInstance().getToken(), list.get(position).getId());
        printcall.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String base) {
                Gson gson = new Gson();
                Unprintnum unprintnum = gson.fromJson(base, Unprintnum.class);
                if (listener == null) {
                    return;
                }
                listener.onItemClick(unprintnum.getUnprintnum());
                print(position);
                list.get(position).setPrint(1);
                notifyDataSetChanged();
            }


            @Override
            public void onError(int code, String error) {
                Toast.makeText(AccountApplication.getInstance(), error,
                        Toast.LENGTH_SHORT).show();
            }
        });
//        printcall.enqueue(new MyCallback<BaseBean<Unprintnum>>() {
//            @Override
//            public void onSuccess(Call<BaseBean<Unprintnum>> call,
//                                  Response<BaseBean<Unprintnum>> response) {
//                BaseBean<Unprintnum> bean=response.body();
//                if(listener==null){
//                    return;
//                }
//                listener.onItemClick(bean.getData().getUnprintnum());
//                print(position);
//                list.get(position).setPrint(1);
//                notifyDataSetChanged();
//            }
//
//            @Override
//            public void onError(int code, String error) {
//                Toast.makeText(AccountApplication.getInstance(),error,
//                        Toast.LENGTH_SHORT).show();
//            }
//        });
//        printTextWithFont
    }

    private void print(int position) {
        LinkedList<TableBean> tabelist = new LinkedList<>();
        String[] text;
//         Log.e("lh_orderAdapter", "list.size()=="+list.size());

        for (DetailsBean i : list.get(position).getDetails()) {
            text = new String[]{i.getName(), "*" + i.getAmount(), "¥" + i.getPrice()};
            tabelist.add(new TableBean(text));
        }
        AidlUtil.getInstance().myPrint(tabelist, list.get(position).getTotalamount(),
                list.get(position).getTotal(),
                list.get(position).getOrderno(),
                list.get(position).getBook_time(),
                list.get(position).getName(),
                list.get(position).getUsername(),
                list.get(position).getDayno());
    }

    private void notice(final TextView tv_notice, final int position) {
        Call<BaseBean<String>> call = HttpManage.getRequestApi().
                noticeUser(SPManage.getInstance().getToken(), list.get(position).getId());
        call.enqueue(new MyCallback<BaseBean<String>>() {
            @Override
            public void onSuccess(Call<BaseBean<String>> call,
                                  Response<BaseBean<String>> response) {
                tv_notice.setText(R.string.noticed);
                list.get(position).setNotice(1);
                Toast.makeText(AccountApplication.getInstance(), response.body().getMsg() + "",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(int code, String error) {
                Toast.makeText(AccountApplication.getInstance(), error,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public interface OnPrintClickListener {
        void onItemClick(int position);
    }

    private class ViewHandler {
        private TextView tv_takemealId;
        private TextView tv_takemealName;
        private TextView tv_takemealMoney;
        private TextView tv_orderTime;
        private TextView tv_takemealPrint;
        private TextView tv_takemealNotice;
    }
}
