package com.kinon.ceshiaccount.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.constant.Constant;
import com.kinon.ceshiaccount.util.SPManage;

/**
 * Created by luohao on 2017-10-31.
 */

public class SetActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_back;
    private SeekBar sb_screenTime;
    private TextView tv_sbTime;
    private EditText et_muneLineNum;
    private EditText mEtTimeInterval;
    private SeekBar.OnSeekBarChangeListener sbListener = new SeekBar.OnSeekBarChangeListener() {
        /**SeekBar滚动时的回调函数*/
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            tv_sbTime.setText((double) i / 10 + "秒");
        }

        /**SeekBar开始滚动的回调函数*/
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        /**SeekBar停止滚动的回调函数*/
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            Constant.TIME_SCREEN_JUMP = seekBar.getProgress() * 100;
            SPManage.getInstance().putICommit("TIME_SCREEN_JUMP", seekBar.getProgress() * 100);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_layout);
        initView();
    }

    private void initView() {
        tv_back = (TextView) findViewById(R.id.tv_back);
        sb_screenTime = (SeekBar) findViewById(R.id.sb_screenTime);
        tv_sbTime = (TextView) findViewById(R.id.tv_sbTime);
        et_muneLineNum = (EditText) findViewById(R.id.et_muneLineNum);
        mEtTimeInterval = findViewById(R.id.et_setting_time_interval);
        sb_screenTime.setProgress(Constant.TIME_SCREEN_JUMP / 100);
        tv_sbTime.setText((double) Constant.TIME_SCREEN_JUMP / 1000 + "秒");
        tv_back.setOnClickListener(this);
        sb_screenTime.setOnSeekBarChangeListener(sbListener);

        int menuNum = SPManage.getInstance().getInt("mune_line_num", 7);
        int timeInterval = SPManage.getInstance().getInt("TIME_INTERVAL", 60);
        et_muneLineNum.setText(String.valueOf(menuNum));
        mEtTimeInterval.setText(String.valueOf(timeInterval));
    }

    @Override
    protected void onPause() {
        super.onPause();
        //保存菜单每行显示个数
        String lineNum = et_muneLineNum.getText().toString().trim();
        String timeInterval = mEtTimeInterval.getText().toString().trim();
        if (!TextUtils.isEmpty(lineNum)) {
            SPManage.getInstance().putICommit("mune_line_num",
                    Integer.parseInt(et_muneLineNum.getText().toString().trim()));
        }

        if (!TextUtils.isEmpty(timeInterval)) {
            SPManage.getInstance().putICommit("TIME_INTERVAL",
                    Integer.parseInt(mEtTimeInterval.getText().toString().trim()));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_back:
                finish();
                break;
            default:
                break;
        }
    }
}
