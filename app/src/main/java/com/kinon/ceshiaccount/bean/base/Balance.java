package com.kinon.ceshiaccount.bean.base;

/**
 * Created by luohao on 2017-08-24.
 * 支付后的余额
 */

public class Balance {
    private double balance;
    private String total;
    private int type;
    private int left;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
