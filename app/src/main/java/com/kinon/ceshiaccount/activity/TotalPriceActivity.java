package com.kinon.ceshiaccount.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.adapter.TotalPriceAdapter;
import com.kinon.ceshiaccount.bean.TotalPriceBean;
import com.kinon.ceshiaccount.bean.TotalTypeBean;
import com.kinon.ceshiaccount.network.HttpManage;
import com.kinon.ceshiaccount.network.StrCallback;
import com.kinon.ceshiaccount.sunmi.print.AidlUtil;
import com.kinon.ceshiaccount.util.MyShowDialog;
import com.kinon.ceshiaccount.util.SPManage;
import com.kinon.ceshiaccount.util.ToastUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2017-12-21.
 */

public class TotalPriceActivity extends BaseActivity implements View.OnClickListener {

    ToastUtil mToast = new ToastUtil();
    private TextView tv_back;
    private TextView tv_totalOrder;
    private TextView tv_totalPrice;
    private GridView gv_total;
    private Button btn_printTotalPrice;
    private String mTotalPrice = "";
    private int mtotalNum = 0;
    private ArrayList<TotalTypeBean> typeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_totalprice_layout);
        initView();
    }

    private void initView() {
        tv_back = (TextView) findViewById(R.id.tv_back);
        tv_totalOrder = (TextView) findViewById(R.id.tv_totalOrder);
        tv_totalPrice = (TextView) findViewById(R.id.tv_totalPrice);
        gv_total = (GridView) findViewById(R.id.gv_total);
        btn_printTotalPrice = (Button) findViewById(R.id.btn_printTotalPrice);

        tv_back.setOnClickListener(this);
        btn_printTotalPrice.setOnClickListener(this);
        getTotalInfo();
    }

    private void getTotalInfo() {
        typeList = new ArrayList<>();
        Call<String> call = HttpManage.getRequestApi()
                .getTotalData(SPManage.getInstance().getToken());
        MyShowDialog.showLoadingDialog(TotalPriceActivity.this, "数据加载中");
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                MyShowDialog.closeLoadingDialog();
                Gson gson = new Gson();
                TotalPriceBean total = gson.fromJson(data, TotalPriceBean.class);
                String totalPrice = String.valueOf(total.getAll_total());
                typeList.addAll(total.getOther());
                tv_totalOrder.setText(String.valueOf(total.getAll_num()));
                tv_totalPrice.setText(String.format("￥%s元", totalPrice));
                TotalPriceAdapter adapter = new TotalPriceAdapter
                        (typeList, TotalPriceActivity.this);
                gv_total.setAdapter(adapter);
                mTotalPrice = totalPrice;
                mtotalNum = total.getAll_num();
            }

            @Override
            public void onError(int code, String error) {
                MyShowDialog.closeLoadingDialog();
                mToast.Short(error);
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.btn_printTotalPrice:
                if (!TextUtils.isEmpty(mTotalPrice)) {
                    AidlUtil.getInstance().printTotalPrice(typeList, mtotalNum, mTotalPrice);
                }
                break;
            default:
                break;
        }
    }
}
