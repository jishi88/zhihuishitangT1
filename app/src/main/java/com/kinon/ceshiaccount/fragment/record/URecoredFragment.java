package com.kinon.ceshiaccount.fragment.record;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.adapter.urgent.U_RecordAdapter;
import com.kinon.ceshiaccount.bean.urgent.U_SettletmentBean;
import com.kinon.ceshiaccount.fragment.BaseFragment;
import com.kinon.ceshiaccount.util.SysTimeManage;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;

/**
 * Created by lhqq on 2017-11-24.
 */

public class URecoredFragment extends BaseFragment {

    private View mView;

    private SmartRefreshLayout smart_record;
//    private RelativeLayout
    /**
     * 记录集合
     */
    private ListView list_record;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_recorddata_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRes();
    }

    private void initRes() {
        smart_record = (SmartRefreshLayout) mView.findViewById(R.id.smart_record);
        list_record = (ListView) mView.findViewById(R.id.list_record);
        //上下拉刷新
        setSmartRefreshLayout();
        //区分紧急模式
        getSettletment();
    }

    /**
     * 紧急情况下运行的代码
     */
    private void getSettletment() {
        long sysTime = System.currentTimeMillis();
        String date = SysTimeManage.getInstage().getDate(sysTime);
        ArrayList<U_SettletmentBean> mDestList =
                (ArrayList<U_SettletmentBean>) DataSupport.where("date=?", date)
                        .order("orderno desc")
                        .find(U_SettletmentBean.class);
        U_RecordAdapter adapter = new U_RecordAdapter(getActivity(), mDestList);
        list_record.setAdapter(adapter);
        Log.e("lh", "mDestList.size()== " + mDestList.size());
    }

    /**
     * 上下拉刷新监听
     */
    private void setSmartRefreshLayout() {
        smart_record.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                smart_record.finishRefresh(2 * 1000);
                getSettletment();

            }
        });
        smart_record.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                smart_record.finishLoadmore(2 * 1000);


            }
        });
    }


}
