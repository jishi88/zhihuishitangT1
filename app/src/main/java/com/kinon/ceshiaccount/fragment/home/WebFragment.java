package com.kinon.ceshiaccount.fragment.home;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.fragment.BaseFragment;
import com.kinon.ceshiaccount.util.SPManage;

/**
 * Created by lhqq on 2017-12-28.
 */

public class WebFragment extends BaseFragment {

    private View mView;
    private WebView web_view;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_web_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initVew();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            showWeb();
        }
    }

    private void initVew() {
        web_view = (WebView) mView.findViewById(R.id.web_view);
        showWeb();
    }

    private void showWeb() {
        String url = SPManage.getInstance().getString("back_url") +
                "index_admin/index?username=" +
                SPManage.getInstance().getString("loginaccoun") +
                "&password="
                + SPManage.getInstance().getString("loginpwd");
        Log.e("lh", "url== " + url);
        web_view.loadUrl(url);
        web_view.setWebViewClient(new WebViewClient() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                web_view.loadUrl(request.getUrl().toString());
                return true;
            }

        });
        WebSettings settings = web_view.getSettings();
        // 缓存(cache)
        settings.setAppCacheEnabled(true);
//        settings.setAppCachePath(context.getCacheDir().getAbsolutePath());
        // 存储(storage)
        settings.setDomStorageEnabled(true);    // 默认值 false
        settings.setDatabaseEnabled(true);      // 默认值 false
        //自适应手机屏幕
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        // 是否支持Javascript，默认值false
        settings.setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // 5.0以上允许加载http和https混合的页面(5.0以下默认允许，5.0+默认禁止)
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);

    }
}
