package com.kinon.ceshiaccount.bean.base;

import com.kinon.ceshiaccount.bean.DetailsBean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-08-23.
 */

public class OrdersBean {
    //订单id
    private String id;
    //订单编号
    private String orderno;
    //当天取餐编号
    private String dayno;
    private String total;
    private String order_time;
    private String book_time;
    //退款
    private int refund;
    //打印
    private int print;
    //通知
    private int notice;
    private String name;
    private int totalamount;
    private String username;
    private ArrayList<DetailsBean> details;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getDayno() {
        return dayno;
    }

    public void setDayno(String dayno) {
        this.dayno = dayno;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    /**
     * 退款
     */
    public int getRefund() {
        return refund;
    }

    public void setRefund(int refund) {
        this.refund = refund;
    }

    /**
     * 打印
     */
    public int getPrint() {
        return print;
    }

    public void setPrint(int print) {
        this.print = print;
    }

    /**
     * 通知
     */
    public int getNotice() {
        return notice;
    }

    public void setNotice(int notice) {
        this.notice = notice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<DetailsBean> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<DetailsBean> details) {
        this.details = details;
    }

    public String getBook_time() {
        return book_time;
    }

    public void setBook_time(String book_time) {
        this.book_time = book_time;
    }

    public int getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(int totalamount) {
        this.totalamount = totalamount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
