package com.kinon.ceshiaccount.bean.urgent;

import org.litepal.crud.DataSupport;

/**
 * Created by luohao on 2017-10-22.
 */

public class Card extends DataSupport {

    private String no;
    private String finger;
    private String name;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getFinger() {
        return finger;
    }

    public void setFinger(String finger) {
        this.finger = finger;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
