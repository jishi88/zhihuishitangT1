package com.kinon.ceshiaccount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-07-17.
 * 计算历史适配器
 */

public class SettlementAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> list;

    public SettlementAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHandle handle;
        if (view == null) {
            handle = new ViewHandle();
            view = LayoutInflater.from(context).
                    inflate(R.layout.item_settlement_layout, null);
            handle.tv_moneyNum = (TextView) view.findViewById(R.id.tv_moneyNum);
            view.setTag(handle);
        } else {
            handle = (ViewHandle) view.getTag();
        }
        handle.tv_moneyNum.setText(list.get(position));
        return view;
    }

    private class ViewHandle {
        TextView tv_moneyNum;
    }
}
