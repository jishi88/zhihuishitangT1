package com.kinon.ceshiaccount.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;

/**
 * Created by luohao on 2017-07-18.
 * 联系客服，显示客服电话
 */

public class CustomerServiceActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customerservice_layout);
        initView();
    }

    private void initView() {
        tv_back = (TextView) findViewById(R.id.tv_back);
        tv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
