package com.kinon.ceshiaccount.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.bean.menu.Breakfast;

import java.util.ArrayList;

/**
 * Created by lhqq on 2017-12-08.
 */

public class MenuAdapter extends BaseAdapter {

    private ArrayList<Breakfast> list;
    private Context mContext;
    private int mWidth = 110;
    private String mColor[] = {"#f63333", "#f6c633", "#56cfc7",
            "#6076ff", "#cf56a0", "#a974f6", "#f68633",};

    public MenuAdapter(ArrayList<Breakfast> breakfast,
                       Context mContext, int mWidth) {
        this.list = breakfast;
        this.mContext = mContext;
        this.mWidth = mWidth;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHandl handl;
        if (view == null) {
            handl = new ViewHandl();
            view = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_menu_layout, null);
            handl.rl_dishes = (RelativeLayout) view.findViewById(R.id.rl_dishes);
            handl.tv_price = (TextView) view.findViewById(R.id.tv_price);
            handl.tv_dishesName = (TextView) view.findViewById(R.id.tv_dishesName);

            FrameLayout.LayoutParams fllp = new FrameLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            fllp.width = mWidth;
            fllp.height = mWidth;
            handl.rl_dishes.setLayoutParams(fllp);

            view.setTag(handl);
        } else {
            handl = (ViewHandl) view.getTag();
        }
//        ImageLoaderManager.getInstance(mContext)
//                .displayImage(handl.img_dishes,list.get(i).getImage());
//        ImageUtils.displayImage(mContext,list.get(i).getImage(),
//                handl.img_dishes,R.drawable.img_imgloading);
        handl.tv_price.setText("￥" + list.get(i).getPrice() + "");
        handl.tv_dishesName.setText(list.get(i).getName());

//        handl.tv_price.setBackgroundColor(Color.parseColor
//                (mColor[list.get(i).getCategoryid()%7]));
        int price = (int) list.get(i).getPrice();
        handl.tv_price.setTextColor(Color.parseColor(mColor[price % 7]));
//        if(price<10){
//            handl.tv_price.setTextColor(Color.parseColor(mColor[0]));
//        }else if(10<price && price<16){
//            handl.tv_price.setTextColor(Color.parseColor(mColor[1]));
//
//        }else if(16<price && price<21){
//            handl.tv_price.setTextColor(Color.parseColor(mColor[2]));
//        }else if(21<price ){
//            handl.tv_price.setTextColor(Color.parseColor(mColor[3]));
//
//        }


//        if(list.get(i).getAmount()==0){
//            handl.tv_num.setVisibility(View.GONE);
//        }else{
//            handl.tv_num.setText(list.get(i).getAmount()+"");
//            handl.tv_num.setVisibility(View.VISIBLE);
//        }
//        handl.rl_good.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                handl.tv_num.setVisibility(View.VISIBLE);
//                handl.tv_num.setText("1");
//            }
//        });

        return view;
    }

    public void setmWidth(int mWidth) {
        this.mWidth = mWidth;
    }

    private class ViewHandl {
        RelativeLayout rl_dishes;
        TextView tv_price;
        TextView tv_dishesName;


    }
}
