package com.kinon.ceshiaccount.fragment.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.activity.CustomerServiceActivity;
import com.kinon.ceshiaccount.activity.LoginActivity;
import com.kinon.ceshiaccount.activity.SetActivity;
import com.kinon.ceshiaccount.activity.StaffInfoActivity;
import com.kinon.ceshiaccount.activity.TotalPriceActivity;
import com.kinon.ceshiaccount.activity.UrgentSetActivity;
import com.kinon.ceshiaccount.constant.Constant;
import com.kinon.ceshiaccount.fragment.BaseFragment;
import com.kinon.ceshiaccount.util.AppInfoUtil;
import com.kinon.ceshiaccount.util.SPManage;
import com.kinon.ceshiaccount.view.CustomDialog;

/**
 * Created by luohao on 2017-06-16.
 * 设置界面主要包括查看工作人员，联系客服，退出登录
 */

public class SetFragment extends BaseFragment implements View.OnClickListener {
    private View mView;
    /**
     * 工作人员
     */
    private Button btn_staff;
    /**
     * 联系客服
     */
    private Button btn_service;
    /**
     * 退出登录
     */
    private Button btn_signout;
    /**
     * 紧急模式设置
     */
    private Button btn_urgentSet;
    private Button btn_voice;
    private Button btn_set;
    private Button btn_total;
    private TextView tv_sersion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_set_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        btn_staff = (Button) mView.findViewById(R.id.btn_staff);
        btn_service = (Button) mView.findViewById(R.id.btn_service);
        btn_signout = (Button) mView.findViewById(R.id.btn_signout);
        btn_urgentSet = (Button) mView.findViewById(R.id.btn_urgentSet);
        btn_voice = (Button) mView.findViewById(R.id.btn_voice);
        btn_set = (Button) mView.findViewById(R.id.btn_set);
        btn_total = (Button) mView.findViewById(R.id.btn_total);
        tv_sersion = (TextView) mView.findViewById(R.id.tv_sersion);
        tv_sersion.setText("版本号：" + AppInfoUtil.getVersionName());
//        btn_urgentSet.setVisibility(View.GONE);
        if (Constant.IS_OPEN_VOICE) {
            btn_voice.setBackgroundResource(R.drawable.btn_set_voiceon);
        }

        btn_staff.setOnClickListener(this);
        btn_service.setOnClickListener(this);
        btn_signout.setOnClickListener(this);
        btn_urgentSet.setOnClickListener(this);
        btn_voice.setOnClickListener(this);
        btn_set.setOnClickListener(this);
        btn_total.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_staff:
                Intent intentStall = new Intent(getActivity(),
                        StaffInfoActivity.class);
                startActivity(intentStall);
                break;
            case R.id.btn_service:
                Intent intentService = new Intent(getActivity(),
                        CustomerServiceActivity.class);
                startActivity(intentService);
                break;
            case R.id.btn_signout:
                CustomDialog dialog = new CustomDialog(getActivity(), null, "是否退出登录", null, null,
                        new CustomDialog.DialogClickListener() {
                            @Override
                            public void onDialogClick(int btn) {
                                if (btn == 1) {
                                    SPManage.getInstance().removeKey("token");
                                    SPManage.getInstance().removeKey("loginaccoun");
                                    SPManage.getInstance().removeKey("loginpwd");
                                    SPManage.getInstance().removeKey("roles");
                                    SPManage.getInstance().removeKey("urlbean");
                                    SPManage.getInstance().removeKey("serverUrl");
                                    Intent intent = new Intent(getActivity(),
                                            LoginActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            }
                        });
                dialog.show();

                break;
            case R.id.btn_urgentSet:
                Intent intent_U = new Intent(getActivity(),
                        UrgentSetActivity.class);
                startActivity(intent_U);
                break;
            case R.id.btn_voice:
                if (Constant.IS_OPEN_VOICE) {
                    btn_voice.setBackgroundResource(R.drawable.btn_set_voiceoff);
                    Toast.makeText(mContext, "语音已关闭", Toast.LENGTH_SHORT).show();
                    Constant.IS_OPEN_VOICE = false;
                    SPManage.getInstance().putBCommit("IS_OPEN_VOICE", Constant.IS_OPEN_VOICE);
                } else {
                    btn_voice.setBackgroundResource(R.drawable.btn_set_voiceon);
                    Toast.makeText(mContext, "语音已开启", Toast.LENGTH_SHORT).show();
                    Constant.IS_OPEN_VOICE = true;
                    SPManage.getInstance().putBCommit("IS_OPEN_VOICE", Constant.IS_OPEN_VOICE);
                }
                break;
            case R.id.btn_set:
                Intent intentSet = new Intent(getActivity(), SetActivity.class);
                startActivity(intentSet);
                break;
            case R.id.btn_total:
                Intent intentT = new Intent(getActivity(), TotalPriceActivity.class);
                startActivity(intentT);
                break;
        }
    }
}
