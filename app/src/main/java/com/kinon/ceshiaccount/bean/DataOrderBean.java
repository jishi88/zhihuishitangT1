package com.kinon.ceshiaccount.bean;

import com.kinon.ceshiaccount.bean.base.OrdersBean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-07-19.
 * 订单列表的实体类
 */

public class DataOrderBean {
    private ArrayList<OrdersBean> orders;
    private int unprintnum;

    public ArrayList<OrdersBean> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<OrdersBean> orders) {
        this.orders = orders;
    }

    public int getUnprintnum() {
        return unprintnum;
    }

    public void setUnprintnum(int unprintnum) {
        this.unprintnum = unprintnum;
    }


//    public class Orders{
//        //订单id
//        private String id;
//        //订单编号
//        private String orderno;
//        //当天取餐编号
//        private String dayno;
//        private String total;
//        private String order_time;
//        //退款
//        private int refund;
//        //打印
//        private int print;
//        //通知
//        private int notice;
//        private String name;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getOrderno() {
//            return orderno;
//        }
//
//        public void setOrderno(String orderno) {
//            this.orderno = orderno;
//        }
//
//        public String getDayno() {
//            return dayno;
//        }
//
//        public void setDayno(String dayno) {
//            this.dayno = dayno;
//        }
//
//        public String getTotal() {
//            return total;
//        }
//
//        public void setTotal(String total) {
//            this.total = total;
//        }
//
//        public String getOrder_time() {
//            return order_time;
//        }
//
//        public void setOrder_time(String order_time) {
//            this.order_time = order_time;
//        }
//         /**退款*/
//        public int getRefund() {
//            return refund;
//        }
//
//        public void setRefund(int refund) {
//            this.refund = refund;
//        }
//         /**打印*/
//        public int getPrint() {
//            return print;
//        }
//
//        public void setPrint(int print) {
//            this.print = print;
//        }
//         /**通知*/
//        public int getNotice() {
//            return notice;
//        }
//
//        public void setNotice(int notice) {
//            this.notice = notice;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//    }
}
