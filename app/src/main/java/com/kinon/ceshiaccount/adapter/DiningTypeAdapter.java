package com.kinon.ceshiaccount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.bean.RoleBean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-09-07.
 * 就餐类型的适配器
 */

public class DiningTypeAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<RoleBean> list;
    private int previousIndex = 0;

    public DiningTypeAdapter(Context context, ArrayList<RoleBean> list) {
        this.context = context;
        this.list = list;
    }

    public DiningTypeAdapter(Context context, ArrayList<RoleBean> list, int select) {
        this.context = context;
        this.list = list;
        previousIndex = select;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHandle handle;
        if (view == null) {
            handle = new ViewHandle();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.item_diningtype_layout, null);
            handle.tv_diningType = (TextView) view.findViewById(R.id.tv_diningType);
            handle.tv_diningType.setText(list.get(i).getCname());

            view.setTag(handle);
        } else {
            handle = (ViewHandle) view.getTag();
        }
        if (previousIndex == i) {
            handle.tv_diningType.setTextColor
                    (context.getResources().getColor(R.color.white));
            handle.tv_diningType.setBackgroundResource(R.drawable.bg_diningtyoe_red);
        } else {
            handle.tv_diningType.setTextColor
                    (context.getResources().getColor(R.color.black_4d4d4d));
            handle.tv_diningType.setBackgroundResource(R.drawable.bg_diningtype);
        }
//        handle.tv_diningType.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(previousIndex==i){
//                    return;
//                }
//                previousIndex=i;
//                notifyDataSetChanged();
//            }
//        });
        return view;
    }

    public void selectItem(int position) {
        previousIndex = position;
    }

    private class ViewHandle {
        TextView tv_diningType;

    }


}
