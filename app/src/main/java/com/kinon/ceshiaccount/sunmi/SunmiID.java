package com.kinon.ceshiaccount.sunmi;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Created by luohao on 2017-08-29.
 */

public class SunmiID {

    public String getSunmiSN(Context context) {
        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        String sn = tm.getSimSerialNumber();
        return sn;
    }

}
