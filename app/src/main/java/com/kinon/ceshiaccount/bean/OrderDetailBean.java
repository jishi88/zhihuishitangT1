package com.kinon.ceshiaccount.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by luohao on 2017-08-22.
 * 取餐时的订单详情
 */

public class OrderDetailBean implements Serializable {
    private int id;
    private String orderno;
    private int dayno;
    private int userid;
    private String order_time;
    private String book_time;
    private String take_time;
    private int pay_type;
    private String pay_time;
    private double total;
    private int state;
    private String note;
    private String pic;
    private int notice;
    private int print;
    private String name;
    private String username;
    private ArrayList<DetailsBean> details;
    private int totalamount;


    public OrderDetailBean(int id, String orderno, int dayno, int userid,
                           String order_time, String book_time, String take_time,
                           int pay_type, String pay_time, double total, int state,
                           String note, String pic, int notice, int print, String name,
                           String username, ArrayList<DetailsBean> details,
                           int totalamount) {
        this.id = id;
        this.orderno = orderno;
        this.dayno = dayno;
        this.userid = userid;
        this.order_time = order_time;
        this.book_time = book_time;
        this.take_time = take_time;
        this.pay_type = pay_type;
        this.pay_time = pay_time;
        this.total = total;
        this.state = state;
        this.note = note;
        this.pic = pic;
        this.notice = notice;
        this.print = print;
        this.name = name;
        this.username = username;
        this.details = details;
        this.totalamount = totalamount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public int getDayno() {
        return dayno;
    }

    public void setDayno(int dayno) {
        this.dayno = dayno;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getBook_time() {
        return book_time;
    }

    public void setBook_time(String book_time) {
        this.book_time = book_time;
    }

    public String getTake_time() {
        return take_time;
    }

    public void setTake_time(String take_time) {
        this.take_time = take_time;
    }

    public int getPay_type() {
        return pay_type;
    }

    public void setPay_type(int pay_type) {
        this.pay_type = pay_type;
    }

    public String getPay_time() {
        return pay_time;
    }

    public void setPay_time(String pay_time) {
        this.pay_time = pay_time;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getNotice() {
        return notice;
    }

    public void setNotice(int notice) {
        this.notice = notice;
    }

    public int getPrint() {
        return print;
    }

    public void setPrint(int print) {
        this.print = print;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(int totalamount) {
        this.totalamount = totalamount;
    }

    public ArrayList<DetailsBean> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<DetailsBean> details) {
        this.details = details;
    }
}
