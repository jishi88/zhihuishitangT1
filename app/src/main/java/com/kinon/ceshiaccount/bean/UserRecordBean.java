package com.kinon.ceshiaccount.bean;

/**
 * Created by lhqq on 2017-12-21.
 */

public class UserRecordBean {
    private String date;
    private String name;
    private String num;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
}
