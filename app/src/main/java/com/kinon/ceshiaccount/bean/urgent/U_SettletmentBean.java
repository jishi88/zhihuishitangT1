package com.kinon.ceshiaccount.bean.urgent;

import org.litepal.crud.DataSupport;

/**
 * Created by luohao on 2017-07-25.
 */

public class U_SettletmentBean extends DataSupport {
    //订单编号
    private String orderno;
    //卡号
    private String card;
    //0==早餐 1==中餐 2==晚餐
    private int type;
    private String total;
    private String time;
    private int status;
    private int num;
    //0==正常，1==申请退款
    private int refund;
    private String date;
    //0==未提交 1==提交
    private int submit;
    //订单识别号
    private String stamp;
    //就餐类型  0=结算 1=就餐 2=餐单
    private int ctype;
    //餐单详情
    private String detail;

    public U_SettletmentBean(String orderno, String card, int type, String total,
                             String time, int status, int num, int refund,
                             String date, int submit) {
        this.orderno = orderno;
        this.card = card;
        this.type = type;
        this.total = total;
        this.time = time;
        this.status = status;
        this.num = num;
        this.refund = refund;
        this.date = date;
        this.submit = submit;
    }

    public U_SettletmentBean(String orderno, String card, int type, String total,
                             String time, int status, int num, int refund,
                             String date, int submit, String stamp) {
        this.orderno = orderno;
        this.card = card;
        this.type = type;
        this.total = total;
        this.time = time;
        this.status = status;
        this.num = num;
        this.refund = refund;
        this.date = date;
        this.submit = submit;
        this.stamp = stamp;
    }

    public U_SettletmentBean(String orderno, String card, int type,
                             String total, String time, int status,
                             int num, int refund, String date,
                             int submit, String stamp, int ctype,
                             String detail) {
        this.orderno = orderno;
        this.card = card;
        this.type = type;
        this.total = total;
        this.time = time;
        this.status = status;
        this.num = num;
        this.refund = refund;
        this.date = date;
        this.submit = submit;
        this.stamp = stamp;
        this.ctype = ctype;
        this.detail = detail;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getRefund() {
        return refund;
    }

    public void setRefund(int refund) {
        this.refund = refund;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSubmit() {
        return submit;
    }

    public void setSubmit(int submit) {
        this.submit = submit;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public int getCtype() {
        return ctype;
    }

    public void setCtype(int ctype) {
        this.ctype = ctype;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "U_SettletmentBean{" +
                "orderno='" + orderno + '\'' +
                ", card='" + card + '\'' +
                ", type=" + type +
                ", total='" + total + '\'' +
                ", time='" + time + '\'' +
                ", status=" + status +
                ", num=" + num +
                ", refund=" + refund +
                ", date='" + date + '\'' +
                ", submit=" + submit +
                ", stamp='" + stamp + '\'' +
                ", ctype=" + ctype +
                ", detail='" + detail + '\'' +
                '}';
    }
}
