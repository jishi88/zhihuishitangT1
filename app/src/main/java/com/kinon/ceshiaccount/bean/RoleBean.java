package com.kinon.ceshiaccount.bean;

/**
 * Created by luohao on 2017-09-08.
 */

public class RoleBean {

    private int id;
    private String cname;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
