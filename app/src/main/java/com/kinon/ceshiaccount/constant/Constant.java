package com.kinon.ceshiaccount.constant;

/**
 * Created by luohao on 2017-06-16.
 * app信息配置类
 */

public class Constant {

    /**
     * 首页时间线程休眠时间
     */
    public static final int TIME_THREAD_SLEEP = 1 * 1000;
    /**是否是离线登陆*/
//    public static boolean URGENT_TYPE=false;
    /**
     * 线程发送的消息
     */
    public static final int TIME_THREAD_MESSAGE = 1;
    /**
     * 权限相关
     */
    public static final int READ_PHONE_STATE_CODE = 0x01;
    public static final int WRITE_EXTERNAL_STORAGE = 0x02;
    /**
     * 跳转类
     */
    public static final int INTENT_CLASS_TAKE = 0X01;
    public static final int INTENT_CLASS_WXPAY = 0X02;
    //网络错误相关
    public static final int NETWORK_NO_FIND = 404;
    public static final int ERROR_JSONE = 1000;
    public static final int ERROR_TIMEOUT_CODE = 1001; //请求超时
    public static final int ERROR_NETWORK = 1002;
    public static final String SHOW_SCREENTAB_TITLE = "取餐商品列表";
    public static final int COMPUTER_NUM_0 = 0;
    public static final int COMPUTER_NUM_1 = 1;
    public static final int COMPUTER_NUM_2 = 2;
    public static final int COMPUTER_NUM_3 = 3;
    public static final int COMPUTER_NUM_4 = 4;
    public static final int COMPUTER_NUM_5 = 5;
    public static final int COMPUTER_NUM_6 = 6;
    public static final int COMPUTER_NUM_7 = 7;
    public static final int COMPUTER_NUM_8 = 8;
    public static final int COMPUTER_NUM_9 = 9;
    public static final int COMPUTER_NUM_POINT = 10;
    public static final int COMPUTER_NUM_PULS = 11;
    public static final int COMPUTER_NUM_C = 12;
    public static final int COMPUTER_NUM_COMPLETE = 0;
    /**
     * 紧急状态 非紧急状态
     */
    public static boolean URGENT_TYPE = false;
    /**
     * 支付需要传的参数
     */
    public static int ACCESS_PAY_TYPE = 0;
    public static boolean IS_OPEN_VOICE = false;
    public static int TIME_SCREEN_JUMP = 100;
    /**
     * 是否删除sp
     */
    public static boolean IS_DELETE_SP = true;
    public static int CARD_LENGTH = 8;
}
