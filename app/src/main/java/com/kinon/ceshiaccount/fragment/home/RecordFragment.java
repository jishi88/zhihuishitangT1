package com.kinon.ceshiaccount.fragment.home;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.adapter.viewpager.RecordViewPAdapter;
import com.kinon.ceshiaccount.application.AccountApplication;
import com.kinon.ceshiaccount.fragment.BaseFragment;
import com.kinon.ceshiaccount.fragment.record.NRecoredFragment;
import com.kinon.ceshiaccount.fragment.record.URecoredFragment;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-06-16.
 * 记录午餐信息，并且可以申请退款
 */

public class RecordFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener {
    private View mView;
    private ViewPager vp_recordType;
    private ImageView img_recordLine;
    private RadioGroup rg_recordType;
    private int width = 1920;


    private Context context;
    private ViewPager.OnPageChangeListener pagerListener = new
            ViewPager.OnPageChangeListener() {

                @Override
                public void onPageScrolled(int position, float positionOffset,
                                           int positionOffsetPixels) {
                    LinearLayout.LayoutParams llp = (LinearLayout.LayoutParams)
                            img_recordLine.getLayoutParams();
                    llp.leftMargin = (int) (width / 2 * (position + positionOffset));
                    img_recordLine.setLayoutParams(llp);
//            getPix();
                }

                @Override
                public void onPageSelected(int position) {
                    RadioButton rb = (RadioButton) rg_recordType.getChildAt(position);
                    rb.setChecked(true);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_record_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
    }
//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if(!hidden){
//            urgentSet();
//        }
//    }

    private void initView() {
        context = AccountApplication.getInstance();

        vp_recordType = (ViewPager) mView.findViewById(R.id.vp_recordType);
        img_recordLine = (ImageView) mView.findViewById(R.id.img_recordLine);
        rg_recordType = (RadioGroup) mView.findViewById(R.id.rg_recordType);
        vp_recordType.setOnPageChangeListener(pagerListener);
        rg_recordType.setOnCheckedChangeListener(this);
        getWindowWith();
        showDataFragment();

    }

    private void showDataFragment() {
        ArrayList<Fragment> fragmentLit = new ArrayList<>();
        fragmentLit.add(new NRecoredFragment());
        fragmentLit.add(new URecoredFragment());
        RecordViewPAdapter adapter = new
                RecordViewPAdapter(getFragmentManager(), fragmentLit);
        vp_recordType.setAdapter(adapter);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rb_recordN:
                vp_recordType.setCurrentItem(0);
                break;
            case R.id.rb_recordU:
                vp_recordType.setCurrentItem(1);
                break;
            default:
                break;
        }
    }

    /**
     * 获取屏幕宽度
     */
    private void getWindowWith() {
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        width = dm.widthPixels;
        getPix();
    }

    /**
     * 初始化横线的位置
     */
    private void getPix() {
        int imgWidth = width / 2;
        LinearLayout.LayoutParams llp = (LinearLayout.LayoutParams)
                img_recordLine.getLayoutParams();
        llp.width = imgWidth;
        img_recordLine.setLayoutParams(llp);
    }

}
