package com.kinon.ceshiaccount.bean;

import java.util.ArrayList;

/**
 * Created by lhqq on 2017-12-21.
 */

public class UserInfoBean {
    private UserBean user;
    private ArrayList<UserRecordBean> records;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public ArrayList<UserRecordBean> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<UserRecordBean> records) {
        this.records = records;
    }
}
