package com.kinon.ceshiaccount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.bean.RoleBean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-09-07.
 * 就餐类型的适配器
 */

public class PayTypeAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<RoleBean> list;
    private int previousIndex = 0;


    public PayTypeAdapter(Context context, ArrayList<RoleBean> list) {
        this.context = context;
        this.list = list;
    }

    public PayTypeAdapter(Context context, ArrayList<RoleBean> list, int select) {
        this.context = context;
        this.list = list;
        previousIndex = select;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHandle handle;
        if (view == null) {
            handle = new ViewHandle();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.item_paytype_layout, null);
            handle.tv_type = (TextView) view.findViewById(R.id.tv_type);
            handle.tv_type.setText(list.get(i).getCname());
            view.setTag(handle);
        } else {
            handle = (ViewHandle) view.getTag();
        }
        if (previousIndex == i) {
            handle.tv_type.setTextColor
                    (context.getResources().getColor(R.color.white));
            handle.tv_type.setBackgroundResource(R.drawable.bg_diningtype_down);
        } else {
            handle.tv_type.setTextColor
                    (context.getResources().getColor(R.color.black_4d4d4d));
            handle.tv_type.setBackgroundResource(R.drawable.bg_diningtype_up);
        }
//        handle.btn_diningType.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(previousIndex==i){
//                    return;
//                }
//                previousIndex=i;
//                notifyDataSetChanged();
//            }
//        });
        return view;
    }

    public void selectItem(int position) {
        previousIndex = position;
    }

//    @SuppressLint("NewApi")
//    public void updateBackground(Button btn_view,int position, View view) {
//
//        int backgroundId;
//        int colo;
//        if (previousIndex==position) {
//            btn_view.setTextColor
//                    (context.getResources().getColor(R.color.white));
//            colo=R.color.white;
//            backgroundId = R.drawable.bg_diningtype_down;
//        } else {
//            colo=R.color.black_4d4d4d;
//            backgroundId = R.drawable.bg_diningtype_up;
//        }
////
//        Log.e("lh", "updateBa==" );
//        btn_view.setTextColor(context.getResources().getColor(colo));
//        view.setBackgroundResource(backgroundId);
//    }

    private class ViewHandle {
        TextView tv_type;

    }


}
