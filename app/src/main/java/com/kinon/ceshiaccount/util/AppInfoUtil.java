package com.kinon.ceshiaccount.util;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.kinon.ceshiaccount.application.AccountApplication;

/**
 * Created by lhqq on 2017-12-04.
 * 获取app信息类
 */

public class AppInfoUtil {

    /**
     * 获取版本号
     *
     * @return
     */
    public static String getVersionName() {
        PackageManager packageManager = AccountApplication.getInstance().getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(AccountApplication.getInstance().getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "1.0.0";
    }
}
