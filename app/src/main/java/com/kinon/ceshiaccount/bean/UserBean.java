package com.kinon.ceshiaccount.bean;

/**
 * Created by lhqq on 2017-12-21.
 */

public class UserBean {
    private String name;
    private String balance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
