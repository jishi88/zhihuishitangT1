package com.kinon.ceshiaccount.bean;

/**
 * Created by luohao on 2017-08-23.
 * 打印
 */

public class Unprintnum {
    private int unprintnum;

    public int getUnprintnum() {
        return unprintnum;
    }

    public void setUnprintnum(int unprintnum) {
        this.unprintnum = unprintnum;
    }
}
