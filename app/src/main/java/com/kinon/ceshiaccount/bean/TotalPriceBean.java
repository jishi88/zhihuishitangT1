package com.kinon.ceshiaccount.bean;

import java.util.ArrayList;

/**
 * Created by lhqq on 2017-12-21.
 */

public class TotalPriceBean {

    private int all_num;
    private double all_total;
    private ArrayList<TotalTypeBean> other;

    public int getAll_num() {
        return all_num;
    }

    public void setAll_num(int all_num) {
        this.all_num = all_num;
    }

    public double getAll_total() {
        return all_total;
    }

    public void setAll_total(double all_total) {
        this.all_total = all_total;
    }

    public ArrayList<TotalTypeBean> getOther() {
        return other;
    }

    public void setOther(ArrayList<TotalTypeBean> other) {
        this.other = other;
    }
}
