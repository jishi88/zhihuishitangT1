package com.kinon.ceshiaccount.bean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-09-08.
 */

public class LoginBean {

    public ArrayList<RoleBean> role;
    public ArrayList<Pic> pic;
    public int access;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<RoleBean> getRoles() {
        return role;
    }

    public void setRoles(ArrayList<RoleBean> roles) {
        this.role = roles;
    }

    public ArrayList<Pic> getPic() {
        return pic;
    }

    public void setPic(ArrayList<Pic> pic) {
        this.pic = pic;
    }

    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    @Override
    public String toString() {
        return "LoginBean{" +
                "token='" + token + '\'' +
                ", role=" + role +
                '}';
    }

    public class Pic {
        int id;
        String pic;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }
    }
}
