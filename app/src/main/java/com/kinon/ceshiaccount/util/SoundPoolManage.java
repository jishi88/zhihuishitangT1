package com.kinon.ceshiaccount.util;

import android.media.SoundPool;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.application.AccountApplication;
import com.kinon.ceshiaccount.constant.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by luohao on 2017-10-26.
 */

public class SoundPoolManage {

    public static Map<Integer, Integer> mSpMap = null;
    public static SoundPool mSoundp = null;
    public static SoundPoolManage mSPoolManage = null;

    public SoundPoolManage() {
        SoundPool.Builder spd = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            spd = new SoundPool.Builder();
            spd.setMaxStreams(21); //支持的最小版本
            mSoundp = spd.build(); //创建SoundPool对象
        }
//        spd.setAudioAttributes(null); //转换音频格式
        mSpMap = new HashMap<>();
        mSpMap.put(1, mSoundp.load(AccountApplication.getInstance(), R.raw.pay_success, 1));
        mSpMap.put(2, mSoundp.load(AccountApplication.getInstance(), R.raw.pay_failure, 2));
 /*       mSpMap.put(3,mSoundp.load(AccountApplication.getInstance(), R.raw.take_success,3));
        mSpMap.put(4,mSoundp.load(AccountApplication.getInstance(), R.raw.take_a,4));
        mSpMap.put(5,mSoundp.load(AccountApplication.getInstance(), R.raw.take_b,5));
        mSpMap.put(6,mSoundp.load(AccountApplication.getInstance(), R.raw.take_c,6));
        mSpMap.put(7,mSoundp.load(AccountApplication.getInstance(), R.raw.take_d,7));
        mSpMap.put(8,mSoundp.load(AccountApplication.getInstance(), R.raw.take_e,8));
        mSpMap.put(9,mSoundp.load(AccountApplication.getInstance(), R.raw.take_f,9));*/
    }


    public static SoundPoolManage getInstance() {

        if (mSPoolManage == null || mSoundp == null || mSpMap == null) {
            mSPoolManage = new SoundPoolManage();
        }
        return mSPoolManage;
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    private static void buildSoundP(){
//
//    }

    //播放其中的音频
    public void soundpPlay(int i) {
        if (!Constant.IS_OPEN_VOICE) {
            return;
        }
        //返回的声音ID号，左声道音量设置  一般为0－1，默认填1，右声道音量设置 一般为0－1，默认填1
        //指定播放声音的优先级，数值越高，优先级越大。默认填0，指定是否循环：-1表示无限循环，0表示不循环，其他值表示要重复播放的次数
        //指定播放速率1.0的播放率可以使声音按照其原始频率
        mSoundp.play(mSpMap.get(i), 1, 1, 0, 0, 1f);
    }

    //清除所有的缓存音频
    public void soundpRelease() {
        mSoundp.release();
    }


}
