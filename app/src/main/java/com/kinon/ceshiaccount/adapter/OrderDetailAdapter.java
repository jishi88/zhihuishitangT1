package com.kinon.ceshiaccount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.bean.DetailsBean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-07-19.
 * 显示取餐时的购买的商品适配器
 */

public class OrderDetailAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<DetailsBean> list;

    public OrderDetailAdapter(Context context, ArrayList<DetailsBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
//        Log.e("lh","getView---");
        final ViewHandler handler;
        if (view == null) {
            handler = new ViewHandler();
            view = LayoutInflater.from(context).inflate(R.layout.item_orderdetail_layout, null);
            handler.tv_serialNumber = (TextView) view.findViewById(R.id.tv_serialNumber);
            handler.tv_dishName = (TextView) view.findViewById(R.id.tv_dishName);
            handler.tv_unitPrice = (TextView) view.findViewById(R.id.tv_unitPrice);
            handler.tv_quantity = (TextView) view.findViewById(R.id.tv_quantity);
            handler.tv_money = (TextView) view.findViewById(R.id.tv_money);
            view.setTag(handler);
        } else {
            handler = (ViewHandler) view.getTag();
        }
        handler.tv_serialNumber.setText(list.get(i).getId() + "");
        handler.tv_dishName.setText(list.get(i).getName());
        handler.tv_unitPrice.setText(list.get(i).getPrice() + "");
        handler.tv_quantity.setText(list.get(i).getAmount() + "");
        handler.tv_money.setText(list.get(i).getTotal() + "");
        return view;
    }

    private class ViewHandler {
        private TextView tv_serialNumber;
        private TextView tv_dishName;
        private TextView tv_unitPrice;
        private TextView tv_quantity;
        private TextView tv_money;
    }

}
