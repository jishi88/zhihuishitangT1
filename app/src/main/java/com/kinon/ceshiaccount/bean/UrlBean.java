package com.kinon.ceshiaccount.bean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-08-29.
 */

public class UrlBean {

    private String name;
    private String back_url;
    private String api;
    private ArrayList<Integer> app_function;
    private String size;
    private int card;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public ArrayList<Integer> getApp_function() {
        return app_function;
    }

    public void setApp_function(ArrayList<Integer> app_function) {
        this.app_function = app_function;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getCard() {
        return card;
    }

    public void setCard(int card) {
        this.card = card;
    }

    public String getBack_url() {
        return back_url;
    }

    public void setBack_url(String back_url) {
        this.back_url = back_url;
    }

    @Override
    public String toString() {
        return "UrlBean{" +
                "name='" + name + '\'' +
                ", api='" + api + '\'' +
                ", app_function=" + app_function +
                '}';
    }
}
