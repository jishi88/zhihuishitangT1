package com.kinon.ceshiaccount.bean;

/**
 * Created by lhqq on 2017-11-28.
 * 刷卡用户
 */

public class PhoneUserBean {
    private String name;
    private String username;
    private String balance;
    private String no;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }
}
