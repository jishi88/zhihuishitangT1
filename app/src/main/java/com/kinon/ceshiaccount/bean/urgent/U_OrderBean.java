package com.kinon.ceshiaccount.bean.urgent;

import org.litepal.annotation.Column;
import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by luohao on 2017-07-25.
 * 订单信息
 */

public class U_OrderBean extends DataSupport implements Serializable {

    @Column(unique = true, defaultValue = "unknown")
//       @Column(unique = false)
    private int id;
    private String orderid;
    private String orderno;
    private String dayno;
    private String total;
    private String order_time;
    private String take_time;
    private String book_time;
    //退款
    private int refund;
    //打印 0==未打印 1==打印
    private int print;
    //通知
    private int notice;
    private String name;
    // 取餐状态 2==已完成
    private int state;
    private String username;
    private int submit;


    private ArrayList<U_OrderDetail> details = new ArrayList<U_OrderDetail>();

    public U_OrderBean(int id, String orderid, String orderno, String dayno, String total,
                       String order_time, String take_time, String book_time, int refund, int print,
                       int notice, String name, int state, String username, int submit,
                       ArrayList<U_OrderDetail> details) {
        this.id = id;
        this.orderid = orderid;
        this.orderno = orderno;
        this.dayno = dayno;
        this.total = total;
        this.order_time = order_time;
        this.take_time = take_time;
        this.book_time = book_time;
        this.refund = refund;
        this.print = print;
        this.notice = notice;
        this.name = name;
        this.state = state;
        this.username = username;
        this.submit = submit;
        this.details = details;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getDayno() {
        return dayno;
    }

    public void setDayno(String dayno) {
        this.dayno = dayno;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getTake_time() {
        return take_time;
    }

    public void setTake_time(String take_time) {
        this.take_time = take_time;
    }

    public String getBook_time() {
        return book_time;
    }

    public void setBook_time(String book_time) {
        this.book_time = book_time;
    }

    public int getRefund() {
        return refund;
    }

    public void setRefund(int refund) {
        this.refund = refund;
    }

    public int getPrint() {
        return print;
    }

    public void setPrint(int print) {
        this.print = print;
    }

    public int getNotice() {
        return notice;
    }

    public void setNotice(int notice) {
        this.notice = notice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getSubmit() {
        return submit;
    }

    public void setSubmit(int submit) {
        this.submit = submit;
    }

    public ArrayList<U_OrderDetail> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<U_OrderDetail> details) {
        this.details = details;
    }


    @Override
    public String toString() {
        return "OrderBean{" +
                "id=" + id +
                ", orderid=" + orderid + "-" +
                ", orderno='" + orderno + '\'' +
                ", dayno='" + dayno + '\'' +
                ", total='" + total + '\'' +
                ", order_time='" + order_time + '\'' +
                ", take_time='" + take_time + '\'' +
                ", refund=" + refund +
                ", print=" + print +
                ", notice=" + notice +
                ", name='" + name + '\'' +
                ", state=" + state +
                ", username='" + username + '\'' +
                ", submit=" + submit +
                ", details=" + details +
                '}';
    }
}
