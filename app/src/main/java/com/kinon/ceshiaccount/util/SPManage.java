package com.kinon.ceshiaccount.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.kinon.ceshiaccount.application.AccountApplication;

/**
 * Created by luohao on 2017-07-21.
 */

public class SPManage {
    /**
     * sharedprefenrences的文件名
     */
    private static final String SP_NAME = "a_user";
    private static SharedPreferences sp = null;
    private static SPManage spManage = null;
    private static Editor editor = null;

    private SPManage() {
        Log.e("lh_sp", "SPManage:=== ");
        sp = AccountApplication.getInstance().getSharedPreferences(SP_NAME,
                Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public static SPManage getInstance() {
        if (spManage == null || sp == null || editor == null) {
            spManage = new SPManage();
        }
        return spManage;
    }

    public void spput(String key, String value) {
        editor.putString(key, value);
    }

    /**
     * 获得sp中的String数据
     */
    public String getString(String key) {
        return sp.getString(key, "");
    }

    public boolean getBoolen(String key) {
        return sp.getBoolean(key, false);
    }

    //    public int getInt(String key,int defaul){
//        return sp.getInt(key,defaul);
//    }
    public int getInt(String key, int def) {
        return sp.getInt(key, def);
    }

    public String getToken() {
        return sp.getString("token", "");
    }

    public void putCommit(String key, String value) {
        editor.putString(key, value);
        commit();
    }

    public void putBCommit(String key, boolean value) {
        editor.putBoolean(key, value);
        commit();
    }

    public void putICommit(String key, int value) {
        editor.putInt(key, value);
        commit();
    }


    public void commit() {
        editor.commit();
    }

    /**
     * 清空sp里面的内容
     */
    public void removesp() {
        editor.clear();
        editor.commit();
    }

    /**
     * 删除sp里面的特定字段
     */
    public void removeKey(String key) {
        editor.remove(key);
        editor.commit();
    }


}
