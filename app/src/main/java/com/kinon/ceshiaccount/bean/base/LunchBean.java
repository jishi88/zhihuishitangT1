package com.kinon.ceshiaccount.bean.base;

/**
 * Created by luohao on 2017-07-21.
 * 午餐支付的实体类
 */

public class LunchBean {
    private String name;
    private int num;
    private int allnum;
    private String total;
    private String balance;
    private int type;
    private int left;
    private String no;


    public LunchBean(String name, int num, String total, String balance, int type, int left, String no) {
        this.name = name;
        this.num = num;
        this.total = total;
        this.balance = balance;
        this.type = type;
        this.left = left;
        this.no = no;
    }

    public LunchBean(String name, int num, int allnum, String total,
                     String balance, int type, int left, String no) {
        this.name = name;
        this.num = num;
        this.allnum = allnum;
        this.total = total;
        this.balance = balance;
        this.type = type;
        this.left = left;
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public int getAllnum() {
        return allnum;
    }

    public void setAllnum(int allnum) {
        this.allnum = allnum;
    }
}
