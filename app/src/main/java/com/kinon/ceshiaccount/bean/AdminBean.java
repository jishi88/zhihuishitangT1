package com.kinon.ceshiaccount.bean;

/**
 * Created by luohao on 2017-08-31.
 */

public class AdminBean {
    private String username;
    private String name;
    private String cellphone;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }
}
