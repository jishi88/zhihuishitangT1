package com.kinon.ceshiaccount.fragment.home;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.constant.Constant;
import com.kinon.ceshiaccount.fragment.BaseFragment;
import com.kinon.ceshiaccount.network.HttpManage;
import com.kinon.ceshiaccount.network.StrCallback;
import com.kinon.ceshiaccount.sunmi.SunmiSdkManage;
import com.kinon.ceshiaccount.util.SPManage;
import com.kinon.ceshiaccount.util.SoundPoolManage;
import com.kinon.ceshiaccount.util.ToastUtil;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Response;
import sunmi.ds.callback.ISendCallback;

/**
 * Created by luohao on 2017-06-16.
 * 刷卡取餐
 */

public class CardTakeMealFragment extends BaseFragment
        implements View.OnClickListener {

    ToastUtil toastUtil = new ToastUtil();
    Handler myHandler = new Handler();
    Runnable delayRun = new Runnable() {
        @Override
        public void run() {
            SunmiSdkManage.getInstance().showWelcomeImg();
        }
    };
    private View mView;
    /**
     * 取餐卡号
     */
    private EditText et_cardTake;
    private Context context;
    private Gson gson = null;
    private ISendCallback sunmiBack = new ISendCallback() {
        @Override
        public void onSendSuccess(long taskId) {
            myHandler.postDelayed(delayRun, Constant.TIME_SCREEN_JUMP);
        }

        @Override
        public void onSendFail(int errorId, String errorInfo) {
        }

        @Override
        public void onSendProcess(long totle, long sended) {
        }
    };
    private View.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
            //回车键监听 ACTION_DOWN按下时监听
            if (KeyEvent.KEYCODE_ENTER == keyCode &&
                    KeyEvent.ACTION_DOWN == keyEvent.getAction()) {
//                Log.e("lh", "onKey===");
                getCardTakeInfo();

                return true;
            }
            return false;
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_cardtakemeal_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();

    }

    private void initView() {
        et_cardTake = (EditText) mView.findViewById(R.id.et_cardTake);
        et_cardTake.requestFocus();
        et_cardTake.setOnKeyListener(keyListener);
    }

    @Override
    public void onClick(View view) {

    }

    private synchronized void getCardTakeInfo() {
        String card = et_cardTake.getText().toString().trim();
        et_cardTake.setText("");
        Call<String> call = HttpManage.getRequestApi().
                cardTake(SPManage.getInstance().getToken(), card);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
//                    Intent intent=new Intent();
                try {
                    JSONObject object = new JSONObject(response.body());
                    showScreen("", object.getString("msg"));
                    toastUtil.Short(object.getString("msg"))
                            .setToastBackground(Color.WHITE, 0).show();
                    SoundPoolManage.getInstance().soundpPlay(3);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
                showScreen("", error);
            }
        });
    }

    private void showScreen(String title, String content) {
        if (content.length() > 8) {
            //content=content.substring(0,8)+"\n"+content.substring(8);
            StringBuffer strBuffer = new StringBuffer(content);
            content = strBuffer.insert(8, "\n").toString();
        }
        //Log.e("lh", "content2:=== "+content);
        //SunmiSdkManage.getInstance().showPrice(title,
        //        content, sunmiBack);

        SunmiSdkManage.getInstance().showDoubleLineImg(title, content, sunmiBack);
    }

}
