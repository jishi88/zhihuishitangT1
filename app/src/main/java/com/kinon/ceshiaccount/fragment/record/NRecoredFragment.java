package com.kinon.ceshiaccount.fragment.record;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.adapter.RecordAdapter;
import com.kinon.ceshiaccount.bean.DataOrderBean;
import com.kinon.ceshiaccount.bean.base.OrdersBean;
import com.kinon.ceshiaccount.fragment.BaseFragment;
import com.kinon.ceshiaccount.network.HttpManage;
import com.kinon.ceshiaccount.network.StrCallback;
import com.kinon.ceshiaccount.util.SPManage;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2017-11-24.
 */

public class NRecoredFragment extends BaseFragment {

    /**
     * 每页显示的个数
     */
    private final int page = 10;
    private View mView;
//    private RelativeLayout
    private SmartRefreshLayout smart_record;
    /**
     * 记录集合
     */
    private ListView list_record;
    private RecordAdapter adapter;
    private ArrayList<OrdersBean> orderlist;
    /**
     * 显示的页数下标
     */
    private int offset = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_recorddata_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRes();
    }

    private void initRes() {
        smart_record = (SmartRefreshLayout) mView.findViewById(R.id.smart_record);
        list_record = (ListView) mView.findViewById(R.id.list_record);
        orderlist = new ArrayList<>();
        //上下拉刷新
        setSmartRefreshLayout();
        //区分紧急模式
//        urgentSet();
        orderlist.clear();
        adapter = new RecordAdapter(getActivity(), orderlist);
        list_record.setAdapter(adapter);
        getOrderInfo();
    }

    /**
     * 得到后台的订单列表
     */
    private void getOrderInfo() {
        Call<String> call = HttpManage.getRequestApi().getOrders
                (SPManage.getInstance().getToken(), page + "", offset + "", "1");
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String base) {
                Gson gson = new Gson();
                DataOrderBean order = gson.fromJson(base, DataOrderBean.class);
                orderlist.addAll(order.getOrders());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(int code, String error) {
                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 上下拉刷新监听
     */
    private void setSmartRefreshLayout() {
        smart_record.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                smart_record.finishRefresh(2 * 1000);
                orderlist.clear();
                offset = 0;
                getOrderInfo();

            }
        });
        smart_record.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                smart_record.finishLoadmore(2 * 1000);
                offset += page;
                getOrderInfo();

            }
        });
    }


}
