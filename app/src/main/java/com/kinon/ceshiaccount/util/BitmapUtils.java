package com.kinon.ceshiaccount.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

/**
 * 副屏分辨率为128px*40px
 * 字号13才能保证在padding 5px的情况下上下两行不会重叠
 * 白底黑字
 * 没有做字符超出限制的处理
 *
 * @author WangZhiYao
 * @date 2018/4/12
 */
public class BitmapUtils {

    private static final int IMG_WIDTH = 128;
    private static final int IMG_HEIGHT = 40;
    private static final int TEXT_SIZE = 13;
    private static final int COLOR_TEXT = Color.BLACK;
    private static final int COLOR_BACKGROUND = Color.WHITE;


    private static Bitmap getBitmap() {
        return Bitmap.createBitmap(IMG_WIDTH, IMG_HEIGHT, Bitmap.Config.RGB_565);
    }

    private static Typeface getFontTypeface() {
        return Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
    }

    private static Paint getTextPaint() {
        Paint titlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        titlePaint.setTextSize(TEXT_SIZE);
        titlePaint.setColor(COLOR_TEXT);
        titlePaint.setTextAlign(Paint.Align.LEFT);
        titlePaint.setTypeface(getFontTypeface());
        return titlePaint;
    }

    public static Bitmap getPaymentBitmap(String topText, String bottomText) {
        Bitmap imgBitmap = getBitmap();
        Canvas tempCanvas = new Canvas(imgBitmap);
        tempCanvas.drawColor(COLOR_BACKGROUND);

        Paint topPaint = getTextPaint();
        Rect topRect = new Rect();
        topPaint.getTextBounds(topText, 0, topText.length(), topRect);
        int topX = 5;
        int topY = topRect.height() + 5;
        tempCanvas.drawText(topText, topX, topY, topPaint);

        Paint bottomPaint = getTextPaint();
        Rect bottomRect = new Rect();
        bottomPaint.getTextBounds(bottomText, 0, bottomText.length(), bottomRect);
        int bottomX = imgBitmap.getWidth() - (bottomRect.width() + 5);
        int bottomY = imgBitmap.getHeight() - 5;
        tempCanvas.drawText(bottomText, bottomX, bottomY, bottomPaint);
        return imgBitmap;
    }
}
