package com.kinon.ceshiaccount.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.constant.Constant;

/**
 * Created by luohao on 2017-08-09.
 * 结算完成界面，提示支付成功，和副屏显示金额
 */

public class PayresultActivity extends BaseActivity implements View.OnClickListener {

    Handler finishHadler = new Handler();
    private TextView tv_back;
    private TextView tv_payHint;
    private int finishTime = 5 * 100;
    private Runnable finishRun = new Runnable() {
        @Override
        public void run() {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payresult_layout);
        initView();
    }

    private void initView() {
        tv_back = (TextView) findViewById(R.id.tv_back);
        tv_payHint = (TextView) findViewById(R.id.tv_payHint);
        //接收从OrderPayActivity传过来的余额
        String balance = getIntent().getStringExtra("balance");
        int intentclass = getIntent().getIntExtra("intentclass", 0);
        if (intentclass == Constant.INTENT_CLASS_TAKE) {
            tv_payHint.setText("取餐成功");
        }
        finishHadler.postDelayed(finishRun, finishTime);
        tv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_back:
                finish();
                break;
                default:
                    break;
        }
    }

    private void screenShow() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finishHadler.removeCallbacks(finishRun);
    }
}
