package com.kinon.ceshiaccount.network;

import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

/**
 * Created by luohao on 2017-07-19.
 */

public class ResponseJson {

    public static String getJson(ResponseBody errbody) {
        BufferedSource source = errbody.source();
        try {
            source.request(Long.MAX_VALUE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Buffer buffer = source.buffer();
        String body = buffer.clone().readString(Charset.forName("UTF-8"));
        return body;
    }
}
