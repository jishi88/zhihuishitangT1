package com.kinon.ceshiaccount.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.bean.AdminBean;
import com.kinon.ceshiaccount.network.HttpManage;
import com.kinon.ceshiaccount.network.StrCallback;
import com.kinon.ceshiaccount.util.SPManage;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by luohao on 2017-07-18.
 * 工作人员信息页面，显示工作人员的信息
 */

public class StaffInfoActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_back;
    private CircleImageView img_head;
    private TextView tv_userName;
    private TextView tv_name;
    private TextView tv_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_layout);

        initView();
    }

    private void initView() {

        tv_back = (TextView) findViewById(R.id.tv_back);
        img_head = (CircleImageView) findViewById(R.id.img_head);
        tv_userName = (TextView) findViewById(R.id.tv_userName);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_phone = (TextView) findViewById(R.id.tv_phone);

        getAdmin();
        tv_back.setOnClickListener(this);
    }

    private void getAdmin() {
        Call<String> call = HttpManage.getRequestApi()
                .getAdmin(SPManage.getInstance().getToken());
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String base) {
                Gson gson = new Gson();
                AdminBean user = gson.fromJson(base, AdminBean.class);
                tv_userName.setText(user.getUsername());
                tv_name.setText(user.getName());
                tv_phone.setText(user.getCellphone());
            }

            @Override
            public void onError(int code, String error) {
                Mtoast(error);
            }
        });

//        Call<BaseBean<Object>> call= HttpManage.getRequestApi()
//                .getAdmin(SPManage.getInstance().getToken());
//        call.enqueue(new MyCallback<BaseBean<Object>>() {
//            @Override
//            public void onSuccess(Call<BaseBean<Object>> call,
//                                  Response<BaseBean<Object>> response) {
//                BaseBean<Object> bean=response.body();
//                Gson gson=new Gson();
//                AdminBean user=gson.fromJson(bean.getData().toString(), AdminBean.class);
////                Log.e(TAG, "user=="+bean.getData().toString());
//                tv_userName.setText(user.getUsername());
//                tv_name.setText(user.getName());
//                tv_phone.setText(user.getCellphone());
//            }
//
//            @Override
//            public void onError(int code, String error) {
//              Mtoast(error);
//            }
//        });
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
