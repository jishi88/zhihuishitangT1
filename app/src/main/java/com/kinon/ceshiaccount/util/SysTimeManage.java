package com.kinon.ceshiaccount.util;

import android.text.format.DateFormat;
import android.util.Log;

/**
 * Created by luohao on 2017-07-25.
 */

public class SysTimeManage {

    public static SysTimeManage timeManage = null;

    public static SysTimeManage getInstage() {
        if (timeManage == null) {
            timeManage = new SysTimeManage();
        }
        return timeManage;
    }

    public String getDateTime(long sysTime) {
        CharSequence timeStr = DateFormat.format("yyyy-MM-dd HH:mm:ss", sysTime);
        return timeStr.toString();
    }

    public String getDate(long sysTime) {
        CharSequence timeStr = DateFormat.format("yyyy-MM-dd", sysTime);
        return timeStr.toString();
    }

    public String getOrderId(long sysTime) {
//        CharSequence timeStr= DateFormat.format("yyyyMMddHHmmss", sysTime);
        CharSequence timeStr = DateFormat.format("yyyyMMddHH", sysTime);
        return timeStr + String.valueOf(sysTime).substring(7);
    }

    public String getYMD(long sysTime) {
        CharSequence timeStr = DateFormat.format("yyyyMMdd", sysTime);
        return timeStr + String.valueOf(sysTime);
    }
//    public int getTimeHour(){
//        Time t=new Time();
//        int hour = t.hour;
//    }

    public int getOrderType(long sysTime) {
        CharSequence timeStr = DateFormat.format("HH", sysTime);
//        String hh= timeStr.toString().substring(0,2);
        int hors = Integer.parseInt(timeStr.toString());
        Log.e("lh", "hors== " + hors);

        int type = 0;
        if (hors >= 5 && hors <= 10) {
            type = 0;
        } else if (hors >= 11 && hors <= 13) {
            type = 1;
        } else if (hors >= 16 && hors <= 19) {
            type = 3;
        }
        return type;
    }


}
