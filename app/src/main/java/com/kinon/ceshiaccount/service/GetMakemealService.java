package com.kinon.ceshiaccount.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.kinon.ceshiaccount.bean.base.BaseBean;
import com.kinon.ceshiaccount.bean.urgent.Card;
import com.kinon.ceshiaccount.bean.urgent.U_OrderBean;
import com.kinon.ceshiaccount.bean.urgent.U_OrderDetail;
import com.kinon.ceshiaccount.network.HttpManage;
import com.kinon.ceshiaccount.network.MyCallback;
import com.kinon.ceshiaccount.util.SPManage;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by luohao on 2017-07-31.
 * 获取卡号和订单信息
 */

public class GetMakemealService extends Service {
    private int num = 0;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        getCard(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    private void getCard(final Intent intent) {
        Call<BaseBean<ArrayList<Card>>> ordercall = HttpManage.getRequestApi()
                .backupGetCards(SPManage.getInstance().getToken());
        ordercall.enqueue(new MyCallback<BaseBean<ArrayList<Card>>>() {
            @Override
            public void onSuccess(Call<BaseBean<ArrayList<Card>>> call,
                                  Response<BaseBean<ArrayList<Card>>> response) {
                BaseBean<ArrayList<Card>> bean = response.body();
                if (bean.getData() == null) {
                    return;
                }
                //删除数据库表中的所有数据
                DataSupport.deleteAll(Card.class);
//                DataSupport.saveAll(bean.getData());
                DataSupport.saveAll(bean.getData());
                //缓存指纹
//                for(Card card:bean.getData()){
//                    card.save();
//                }
                getOrder(intent);
//                orderlist.addAll(response.body().getData().getOrders());
//                adapter.notifyDataSetChanged();
//                tv_noPrint.setText("未打印人数:"+response.body().getData().getUnprintnum());
            }

            @Override
            public void onError(int code, String error) {
                getOrder(intent);
//                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 获得订单
     */
    private void getOrder(final Intent intent) {
        if (num >= 3) {
            stopService(intent);
            return;
        }
        Call<BaseBean<ArrayList<U_OrderBean>>> ordercall = HttpManage.getRequestApi()
                .backupGetOrders(SPManage.getInstance().getToken());
        ordercall.enqueue(new MyCallback<BaseBean<ArrayList<U_OrderBean>>>() {
            @Override
            public void onSuccess(Call<BaseBean<ArrayList<U_OrderBean>>> call,
                                  Response<BaseBean<ArrayList<U_OrderBean>>> response) {
                BaseBean<ArrayList<U_OrderBean>> bean = response.body();
                //删除数据库表中的所有数据
                DataSupport.deleteAll(U_OrderBean.class);
//                for(U_OrderBean i:bean.getData()){
//                    i.save();
//                    for (U_OrderDetail j:i.getDetails()){
//                        j.save();
//                    }
//                }
                DataSupport.saveAll(bean.getData());
                ArrayList<U_OrderDetail> tetails = new ArrayList<>();
                for (U_OrderBean o : bean.getData()) {
                    tetails.addAll(o.getDetails());
                }
                DataSupport.saveAll(tetails);
                stopService(intent);

//                orderlist.addAll(response.body().getData().getOrders());
//                adapter.notifyDataSetChanged();
//                tv_noPrint.setText("未打印人数:"+response.body().getData().getUnprintnum());
            }

            @Override
            public void onError(int code, String error) {
                getOrder(intent);
                num++;
//                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });

    }


//    private void stop(){
//        if(num>=3 || twoComplete==2){
//            stopService(mIntent);
//        }
//
//    }


    @Override
    public void onDestroy() {

        super.onDestroy();


    }
}
