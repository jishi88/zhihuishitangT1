package com.kinon.ceshiaccount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.bean.TotalTypeBean;

import java.util.ArrayList;

/**
 * Created by lhqq on 2017-12-21.
 */

public class TotalPriceAdapter extends BaseAdapter {

    private ArrayList<TotalTypeBean> list;
    private Context mContext;

    public TotalPriceAdapter(ArrayList<TotalTypeBean> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHandler handler;
        if (view == null) {
            view = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_total_layout, null);
            handler = new ViewHandler();
            handler.tv_type = (TextView) view.findViewById(R.id.tv_type);
            handler.tv_total = (TextView) view.findViewById(R.id.tv_total);
            handler.tv_unit = (TextView) view.findViewById(R.id.tv_unit);
            view.setTag(handler);
        } else {
            handler = (ViewHandler) view.getTag();
        }
        handler.tv_type.setText(list.get(i).getName());
        handler.tv_total.setText(list.get(i).getTotal());
        handler.tv_unit.setText(list.get(i).getUnit());
        return view;
    }

    private class ViewHandler {
        private TextView tv_type;
        private TextView tv_total;
        private TextView tv_unit;
    }
}
