package com.kinon.ceshiaccount.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.adapter.DiningTypeAdapter;
import com.kinon.ceshiaccount.adapter.OrderDetailAdapter;
import com.kinon.ceshiaccount.adapter.PayTypeAdapter;
import com.kinon.ceshiaccount.bean.DetailsBean;
import com.kinon.ceshiaccount.bean.base.OrdersBean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-07-18.
 * 自定义的dialog
 */

public class CusOrderDialog extends Dialog implements View.OnClickListener {


    /**
     * 确认收货的类型
     */
    private final String order_state = "2";
    private ViewGroup mView;
    private TextView tv_back;
    private TextView tv_name;
    private TextView tv_number;
    private TextView tv_takeTime;
    private ListView list_goods;
    private TextView tv_totalAmount;
    private TextView tv_totalPrice;
    private Button btn_receipt;
    private DialogClickListener listener;
    private PayTypeAdapter diningAdapter = null;
    private DiningTypeAdapter typeAdapter = null;
    private AdapterView.OnItemClickListener onItemclick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//            Log.e("lh","i=="+i);
            diningAdapter.selectItem(i);
            diningAdapter.notifyDataSetChanged();
            listener.onDialogClick(i);
            dismiss();
        }
    };

    public CusOrderDialog(Context context, String msg,
                          DialogClickListener confirmListener) {
        this(0, context, null, msg, null, null, 0, null, 0, confirmListener);
    }

    public CusOrderDialog(int type, Context context, /* String msg,*/
                          DialogClickListener confirmListener) {
        this(type, context, null, null, null, null, 0, null, 0, confirmListener);
    }

    public CusOrderDialog(Context context, String title, String msg,
                          String confirm, String cancel, DialogClickListener confirmListener) {

        this(0, context, title, msg, confirm, cancel, 0, null, 0, confirmListener);
    }

    public CusOrderDialog(int dType, Context context, String title, OrdersBean orders,
                          int index, DialogClickListener confirmListener) {
        this(dType, context, title, null, null, null, 0, orders, index, confirmListener);
    }

    public CusOrderDialog(Context context, OrdersBean orders) {
        this(0, context, null, null, null, null, 0, orders, 0, null);
    }

    public CusOrderDialog(int dType, Context context, String title, String msg, String confirm, String cancel,
                          int msgSize, OrdersBean orders, int index,
                          DialogClickListener listener) {
        super(context, R.style.Base_Theme_AppCompat_Dialog);
        this.listener = listener;
        initDialog(dType, context, title, msg, confirm, cancel, orders, index);
    }

    private void initDialog(int dType, Context context, String title, String message,
                            String confirm, String cancel, OrdersBean orders, int index) {
        setContentView(createDialogView(context, R.layout.activity_orderdetail_layout));
        setParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        tv_back = (TextView) findViewById(R.id.tv_back);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_number = (TextView) findViewById(R.id.tv_number);
        tv_takeTime = (TextView) findViewById(R.id.tv_takeTime);
        list_goods = (ListView) findViewById(R.id.list_goods);
        tv_totalAmount = (TextView) findViewById(R.id.tv_totalAmount);
        tv_totalPrice = (TextView) findViewById(R.id.tv_totalPrice);
        btn_receipt = (Button) findViewById(R.id.btn_receipt);
        btn_receipt.setVisibility(View.GONE);

        tv_back.setOnClickListener(this);
        tv_name.setText(orders.getName());
        tv_number.setText(orders.getDayno() + "");
        tv_takeTime.setText(orders.getBook_time());
        tv_totalAmount.setText("数量:" + orders.getTotalamount());
        tv_totalPrice.setText("合计:" + orders.getTotal());
//        if(orders){
//            btn_receipt.setText("已完成");
//            btn_receipt.setClickable(false);
//        }
        ArrayList<DetailsBean> details = orders.getDetails();
        OrderDetailAdapter adapter = new
                OrderDetailAdapter(context, details);
        list_goods.setAdapter(adapter);

    }

    @Override
    public void onClick(View view) {
//        if(listener==null){
//            dismiss();
//            Log.e("", "listener==null");
//            return;
//        }
        switch (view.getId()) {
            case R.id.tv_back:
//                listener.onDialogClick(0);
                dismiss();

                break;
//            case R.id.tv_dialogYes:
////                listener.onDialogClick(1);
//                dismiss();
//                break;
        }
    }

    public void setParams(int width, int height) {
        WindowManager.LayoutParams dialogParams = this.getWindow().getAttributes();
        dialogParams.width = width;
        dialogParams.height = height;
        this.getWindow().setAttributes(dialogParams);
    }


    /**
     * 找到布局
     *
     * @param layoutId 布局id
     * @return
     */
    private ViewGroup createDialogView(Context context, int layoutId) {
        mView = (ViewGroup) LayoutInflater.from(context).inflate(layoutId, null);
        return mView;
    }

    public View findChildViewById(int id) {
        return mView.findViewById(id);
    }

    public void setDialogClickListener(DialogClickListener listener) {
        this.listener = listener;
    }

    public interface DialogClickListener {
        public void onDialogClick(int btn);
    }


}
