package com.kinon.ceshiaccount.bean.urgent;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

/**
 * Created by luohao on 2017-07-25.
 * 订单详情
 */

public class U_OrderDetail extends DataSupport implements Serializable {
    //    @Column(nullable = false)
//    @Column(unique = false, defaultValue = "id")
    private int id;
    private int amount;
    private String price;
    private String total;
    private String did;
    private String name;
    private String unit;
//    private OrderBean orderBean;


    public U_OrderDetail(int id, int amount, String price, String total,
                         String did, String name, String unit) {
        this.id = id;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.did = did;
        this.name = name;
        this.unit = unit;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

//    public OrderBean getOrderBean() {
//        return orderBean;
//    }
//
//    public void setOrderBean(OrderBean orderBean) {
//        this.orderBean = orderBean;
//    }

    @Override
    public String toString() {
        return "OrderDetail{" +
                "id=" + id +
                ", amount=" + amount +
                ", price='" + price + '\'' +
                ", total='" + total + '\'' +
                ", did='" + did + '\'' +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                '}';
    }
}
