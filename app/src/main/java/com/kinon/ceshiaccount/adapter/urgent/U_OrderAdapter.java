package com.kinon.ceshiaccount.adapter.urgent;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.bean.urgent.U_OrderBean;
import com.kinon.ceshiaccount.bean.urgent.U_OrderDetail;
import com.kinon.ceshiaccount.sunmi.bean.TableBean;
import com.kinon.ceshiaccount.sunmi.print.AidlUtil;
import com.kinon.ceshiaccount.view.CustomDialog;

import java.util.ArrayList;
import java.util.LinkedList;

import static com.kinon.ceshiaccount.R.string.print;

/**
 * Created by luohao on 2017-07-19.
 * 记录中的订单适配器
 */

public class U_OrderAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<U_OrderBean> list;

    private OnPrintClickListener listener = null;

    public U_OrderAdapter(Context context, ArrayList<U_OrderBean> list) {
        this.context = context;
        this.list = list;
    }

    public void setprintClickListener(OnPrintClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
//        Log.e("lh","getView---");
        final ViewHandler handler;
        if (view == null) {
            handler = new ViewHandler();
            view = LayoutInflater.from(context).inflate(R.layout.item_order_layout, null);
            handler.tv_takemealId = (TextView) view.findViewById(R.id.tv_takemealId);
            handler.tv_takemealName = (TextView) view.findViewById(R.id.tv_takemealName);
            handler.tv_takemealMoney = (TextView) view.findViewById(R.id.tv_takemealMoney);
            handler.tv_orderTime = (TextView) view.findViewById(R.id.tv_orderTime);
            handler.tv_takemealPrint = (TextView) view.findViewById(R.id.tv_takemealPrint);
            handler.tv_takemealNotice = (TextView) view.findViewById(R.id.tv_takemealNotice);
            view.setTag(handler);
        } else {
            handler = (ViewHandler) view.getTag();
        }
        handler.tv_takemealId.setText(list.get(i).getDayno());
        handler.tv_takemealName.setText(list.get(i).getName());
        handler.tv_takemealMoney.setText("¥" + list.get(i).getTotal());
        handler.tv_orderTime.setText(list.get(i).getOrder_time());

        //打印
        if (list.get(i).getPrint() == 1) {
            handler.tv_takemealPrint.setText(R.string.printed);
        } else {
            handler.tv_takemealPrint.setText(print);
        }
        //通知
        if (list.get(i).getNotice() == 1) {
            handler.tv_takemealNotice.setText(R.string.noticed);
        } else {
            handler.tv_takemealNotice.setText(R.string.notice);
        }
        //打印的监听事件
        handler.tv_takemealPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.get(i).getPrint() == 1) {
                    printDialog(handler.tv_takemealPrint, i, "是否重复打印");
                } else {
                    print(i, 0);
                }
            }
        });

        return view;
    }

    /**
     * 提示是否重复打印
     */
    private void printDialog(final TextView tv_orderRefunds, final int position, String msg) {
        CustomDialog dialog = new CustomDialog(context, null, msg, null, null,
                new CustomDialog.DialogClickListener() {
                    @Override
                    public void onDialogClick(int btn) {
                        if (btn == 1) {
                            print(position, 1);
//                    refund(tv_orderRefunds,position);
                        }
                    }
                });
        dialog.show();
    }

    private void print(int position, int printtype) {
        LinkedList<TableBean> tabelist = new LinkedList<>();
        String[] text;
        Log.e("lh_orderAdapter", "list.size()==" + list.size());
        int totalAmount = 0;
        for (U_OrderDetail i : list.get(position).getDetails()) {
            text = new String[]{i.getName(), "*" + i.getAmount(), "¥" + i.getPrice()};
            tabelist.add(new TableBean(text));
            totalAmount += i.getAmount();
        }
        AidlUtil.getInstance().myPrint(tabelist, totalAmount,
                list.get(position).getTotal(),
                list.get(position).getOrderno(),
                list.get(position).getBook_time(),
                list.get(position).getName(),
                list.get(position).getUsername(),
                list.get(position).getDayno());

        if (listener != null && printtype == 0) {
            listener.onItemClick(position);
        }

    }

    public interface OnPrintClickListener {
        void onItemClick(int position);
    }

    private class ViewHandler {
        private TextView tv_takemealId;
        private TextView tv_takemealName;
        private TextView tv_takemealMoney;
        private TextView tv_orderTime;
        private TextView tv_takemealPrint;
        private TextView tv_takemealNotice;
    }

}
