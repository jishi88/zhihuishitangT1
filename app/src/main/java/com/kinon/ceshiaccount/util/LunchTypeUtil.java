package com.kinon.ceshiaccount.util;

import android.text.format.DateFormat;
import android.util.Log;

/**
 * Created by luohao on 2017-11-02.
 */

public class LunchTypeUtil {

    public static String lunchType(int type) {
        String strType = "";
        switch (type) {
            case 0:
                strType = "早餐";
                break;
            case 1:
                strType = "午餐";
                break;
            case 2:
                strType = "下午茶";
                break;
            case 3:
                strType = "晚餐";
                break;
            case 4:
                strType = "超市";
                break;
            case 5:
                strType = "理发店";
                break;
            case 6:
                strType = "宵夜";
                break;
            case 7:
                strType = "生鲜";
                break;
            case 8:
                strType = "水果";
                break;
            case 9:
                strType = "线上订单";
                break;
            case 10:
                strType = "外卖";
                break;
            case 11:
                strType = "";
                break;
            case 12:
                strType = "外来人员";
                break;
            case 13:
                strType = "早餐2";
                break;
            default:
                break;
        }
        return strType;
    }

    public static int weekIndex() {
        long sysTime = System.currentTimeMillis();
        CharSequence timeStr = DateFormat.format("EE", sysTime);
        Log.e("lh", "weekIndex==" + timeStr.toString());
        int index = 0;
        switch (timeStr.toString()) {
            case "周一":
                index = 0;
                break;
            case "周二":
                index = 1;
                break;
            case "周三":
                index = 2;
                break;
            case "周四":
                index = 3;
                break;
            case "周五":
                index = 4;
                break;
            case "周六":
                index = 5;
                break;
            case "周日":
                index = 6;
                break;
            default:
                break;
        }
        return index;
    }

}
