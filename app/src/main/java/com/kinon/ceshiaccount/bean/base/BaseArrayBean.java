package com.kinon.ceshiaccount.bean.base;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-08-23.
 */

public class BaseArrayBean<T> {
    private boolean code;
    private String msg;
    private ArrayList<T> data;

    public boolean isCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<T> getData() {
        return data;
    }

    public void setData(ArrayList<T> data) {
        this.data = data;
    }
}
