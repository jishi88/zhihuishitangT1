package com.kinon.ceshiaccount.fragment.home;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.adapter.UserRecordAdapter;
import com.kinon.ceshiaccount.bean.UserInfoBean;
import com.kinon.ceshiaccount.bean.UserRecordBean;
import com.kinon.ceshiaccount.constant.Constant;
import com.kinon.ceshiaccount.fragment.BaseFragment;
import com.kinon.ceshiaccount.network.HttpManage;
import com.kinon.ceshiaccount.network.StrCallback;
import com.kinon.ceshiaccount.util.SPManage;
import com.kinon.ceshiaccount.util.ToastUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2017-12-21.
 */

public class BalanceFragment extends BaseFragment implements View.OnClickListener {

    private View mView;
    private EditText et_card;
    private TextView tv_balance;
    private SmartRefreshLayout smart_userRecord;
    private ListView list_userRecord;
    private Button btn_clearUserInfo;

    private int offset = 0;
    private int page = 10;

    private Gson mGson;
    private ToastUtil mToastu;

    private ArrayList<UserRecordBean> userrecordList;
    private UserRecordAdapter adapter;
    private String mCard = "";
    private TextWatcher tWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence text, int start, int i1, int i2) {
            if (start == Constant.CARD_LENGTH - 1) {

                getBalance(text.toString());
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_balance_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            et_card.setText("");
        }
    }

    private void initView() {
        et_card = (EditText) mView.findViewById(R.id.et_card);
        tv_balance = (TextView) mView.findViewById(R.id.tv_balance);
        smart_userRecord = (SmartRefreshLayout) mView.findViewById(R.id.smart_userRecord);
        list_userRecord = (ListView) mView.findViewById(R.id.list_userRecord);
        btn_clearUserInfo = (Button) mView.findViewById(R.id.btn_clearUserInfo);

        et_card.requestFocus();
        et_card.addTextChangedListener(tWatcher);
        btn_clearUserInfo.setOnClickListener(this);
        //上下拉刷新
        setSmartRefreshLayout();
        mToastu = new ToastUtil();
        mGson = new Gson();
        userrecordList = new ArrayList<>();
        adapter = new UserRecordAdapter(getActivity(), userrecordList);
        list_userRecord.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_clearUserInfo:
                clearInfo();
                break;
                default:
                    break;
        }
    }

    private void clearInfo() {
        tv_balance.setText("");
        mCard = "";
        et_card.setText("");
        if (userrecordList != null) {
            userrecordList.clear();
        }

        adapter.notifyDataSetChanged();
        offset = 0;
        page = 10;
    }

    private void getBalance(String card) {
        synchronized (card) {
            mCard = card;

            et_card.setText("");
            Call<String> call = HttpManage.getRequestApi().getUserByCode
                    (SPManage.getInstance().getToken(), card, page, offset);
            call.enqueue(new StrCallback<String>() {
                @Override
                public void onSuccess(Response<String> response, String data) {
                    userrecordList.clear();
                    UserInfoBean userinfoBean =
                            mGson.fromJson(data, UserInfoBean.class);
                    tv_balance.setText(String.format("%s元", userinfoBean.getUser().getBalance()));
                    userrecordList.addAll(userinfoBean.getRecords());
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onError(int code, String error) {
                    mToastu.Short(error).setToastBackground(Color.WHITE, 0).show();
                    ;
                }
            });
        }
    }

    /**
     * 上下拉刷新监听
     */
    private void setSmartRefreshLayout() {
        smart_userRecord.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                smart_userRecord.finishRefresh(2 * 1000);
                if (!Constant.URGENT_TYPE) {
                    userrecordList.clear();
                    offset = 0;
                    if (!TextUtils.isEmpty(mCard)) {
                        getBalance(mCard);
                    }
                }
            }
        });
        smart_userRecord.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                smart_userRecord.finishLoadmore(2 * 1000);
                if (!Constant.URGENT_TYPE) {
                    offset += page;
                    if (!TextUtils.isEmpty(mCard)) {
                        getBalance(mCard);
                    }

//                    getOrderInfo();
                }
            }
        });
    }


}
