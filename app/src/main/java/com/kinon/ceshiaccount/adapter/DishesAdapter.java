package com.kinon.ceshiaccount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.bean.menu.SelDisches;

import java.util.ArrayList;

/**
 * Created by lhqq on 2017-12-08.
 */

public class DishesAdapter extends BaseAdapter {

    private ArrayList<SelDisches> list;
    private Context mContext;

    private DishesClickListener listener = null;

    public DishesAdapter(ArrayList<SelDisches> breakfast, Context mContext) {
        this.list = breakfast;
        this.mContext = mContext;
    }

    public void setDishesClickListener(DishesClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHandl handl;
        if (view == null) {
            handl = new ViewHandl();
            view = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_dishes_layout, null);
            handl.tv_less = (ImageView) view.findViewById(R.id.tv_less);
            handl.tv_add = (ImageView) view.findViewById(R.id.tv_add);
            handl.tv_dishesName = (TextView) view.findViewById(R.id.tv_dishesName);
            handl.tv_dishesPrice = (TextView) view.findViewById(R.id.tv_dishesPrice);
            handl.tv_dishesNum = (TextView) view.findViewById(R.id.tv_dishesNum);
            view.setTag(handl);
        } else {
            handl = (ViewHandl) view.getTag();
        }
        handl.tv_dishesName.setText(list.get(i).getName());
        handl.tv_dishesPrice.setText("￥" + list.get(i).getPrice() + "");
        handl.tv_dishesNum.setText(list.get(i).getNum() + "");

        handl.tv_less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.ondishesClick(i, 0);
            }
        });
        handl.tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.ondishesClick(i, 1);
            }
        });

        return view;
    }

    public interface DishesClickListener {
        //0代表减1代表加
        void ondishesClick(int position, int type);
    }

    private class ViewHandl {
        ImageView tv_less;
        ImageView tv_add;
        TextView tv_dishesName;
        TextView tv_dishesPrice;
        TextView tv_dishesNum;
    }
}
