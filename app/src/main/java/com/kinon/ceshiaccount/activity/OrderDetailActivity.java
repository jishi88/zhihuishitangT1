package com.kinon.ceshiaccount.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.adapter.OrderDetailAdapter;
import com.kinon.ceshiaccount.adapter.urgent.U_OrderDetailAdapter;
import com.kinon.ceshiaccount.bean.DetailsBean;
import com.kinon.ceshiaccount.bean.OrderDetailBean;
import com.kinon.ceshiaccount.bean.urgent.U_OrderBean;
import com.kinon.ceshiaccount.bean.urgent.U_OrderDetail;
import com.kinon.ceshiaccount.constant.Constant;
import com.kinon.ceshiaccount.network.HttpManage;
import com.kinon.ceshiaccount.network.StrCallback;
import com.kinon.ceshiaccount.sunmi.SunmiSdkManage;
import com.kinon.ceshiaccount.sunmi.bean.ItemBean;
import com.kinon.ceshiaccount.sunmi.bean.KVP;
import com.kinon.ceshiaccount.util.SPManage;
import com.kinon.ceshiaccount.util.SysTimeManage;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by luohao on 2017-08-22.
 * 取餐商品详情界面,还负责确认收货
 */

public class OrderDetailActivity extends BaseActivity implements View.OnClickListener {

    /**
     * 确认收货的类型
     */
    private final String order_state = "2";
    private TextView tv_back;
    private TextView tv_name;
    private TextView tv_number;
    private TextView tv_takeTime;
    private TextView mTvPayType;
    private ListView list_goods;
    private TextView tv_totalAmount;
    private TextView tv_totalPrice;
    private Button btn_receipt;
    private String orderid = null;

    private String dayno = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetail_layout);
        initView();
    }

    private void initView() {
        tv_back = (TextView) findViewById(R.id.tv_back);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_number = (TextView) findViewById(R.id.tv_number);
        tv_takeTime = (TextView) findViewById(R.id.tv_takeTime);
        mTvPayType = findViewById(R.id.tv_pay_type);
        list_goods = (ListView) findViewById(R.id.list_goods);
        tv_totalAmount = (TextView) findViewById(R.id.tv_totalAmount);
        tv_totalPrice = (TextView) findViewById(R.id.tv_totalPrice);
        btn_receipt = (Button) findViewById(R.id.btn_receipt);

        tv_back.setOnClickListener(this);
        btn_receipt.setOnClickListener(this);
        setTextViewContent();
    }

    /**
     * 设置控件的显示内容
     */
    private void setTextViewContent() {
        if (!Constant.URGENT_TYPE) {
            orderdetail();
        } else {
            urgentOrderDetail();
        }
    }

    /**
     * 订单详情
     */
    private void orderdetail() {
        int idType = getIntent().getIntExtra("idType", -1);
        OrderDetailBean bean = (OrderDetailBean) getIntent()
                .getSerializableExtra("orderdetail");
        if (bean == null) {
            return;
        }
        orderid = bean.getId() + "";
        tv_name.setText(bean.getName());
        tv_number.setText(bean.getDayno() + "");
        tv_takeTime.setText(bean.getBook_time());
        String payType;
        switch (bean.getPay_type()) {
            case 3:
                payType = "余额支付";
                break;
            case 4:
                payType = "线下支付";
                mTvPayType.setTextColor(getResources().getColor(R.color.red_633));
                break;
            default:
                payType = "其他方式";
                break;
        }
        mTvPayType.setText(payType);
        tv_totalAmount.setText("数量:" + bean.getTotalamount());
        tv_totalPrice.setText("合计:" + bean.getTotal());
        if (bean.getState() == 2) {
            btn_receipt.setText("已完成");
            btn_receipt.setClickable(false);
        }
        ArrayList<DetailsBean> details = bean.getDetails();
        OrderDetailAdapter adapter = new
                OrderDetailAdapter(getApplicationContext(), details);
        list_goods.setAdapter(adapter);
        screenData(bean.getDayno() + "", "¥" + bean.getTotal(), details, null);


    }

    /**
     * 紧急情况下显示的内容
     */
    private void urgentOrderDetail() {
        ArrayList<U_OrderBean> uOlist = (ArrayList<U_OrderBean>) getIntent()
                .getSerializableExtra("uOlist");
        if (null != uOlist && uOlist.size() > 0) {
            ArrayList<U_OrderDetail> details = uOlist.get(0).getDetails();
            dayno = uOlist.get(0).getDayno();
            tv_name.setText(uOlist.get(0).getName());
            tv_number.setText(dayno);
            tv_takeTime.setText(uOlist.get(0).getTake_time());
            if (uOlist.get(0).getState() == 2) {
                btn_receipt.setText("已完成");
            }
            int amount = 0;
            for (int i = 0; i < details.size(); i++) {
                amount += details.get(i).getAmount();
            }
            tv_totalAmount.setText("数量:" + amount);
            tv_totalPrice.setText("价格:" + "¥" + uOlist.get(0).getTotal());
            list_goods.setAdapter(new U_OrderDetailAdapter(getApplicationContext(), details));
            screenData(amount + "", "¥" + uOlist.get(0).getTotal(), null, details);
        }

    }

    public void screenData(String dayno, String total,
                           ArrayList<DetailsBean> details, ArrayList<U_OrderDetail> uDetails) {
        List<KVP> kvpList = new ArrayList<>();
        kvpList.add(new KVP("编号:", "" + dayno));
        kvpList.add(new KVP("总价:", "" + total));
        List<ItemBean> itemList = new ArrayList<>();
        if (uDetails == null) {
            for (DetailsBean detail : details) {
                itemList.add(new ItemBean(detail.getId() + "", detail.getName(),
                        detail.getPrice() + "", detail.getAmount() + "", detail.getTotal() + ""));
            }
        } else {
            for (U_OrderDetail detail : uDetails) {
                itemList.add(new ItemBean(detail.getId() + "", detail.getName(),
                        detail.getPrice() + "", detail.getAmount() + "", detail.getTotal() + ""));
            }
        }
        SunmiSdkManage.getInstance().showSceerTab(itemList, kvpList, null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.btn_receipt:
                if (!Constant.URGENT_TYPE) {
                    receipt();
                } else {
                    u_receipt();
                }
                break;
        }
    }

    private void u_receipt() {
        if (btn_receipt.getText().toString().trim().equals("已完成")) {
            Mtoast("订单不是待取餐状态,无法完成");
            return;
        }
        long sysTime = System.currentTimeMillis();
        String time = SysTimeManage.getInstage().getDateTime(sysTime);
        ContentValues values = new ContentValues();
        values.put("state", 2);
        values.put("take_time", time);
        DataSupport.updateAll(U_OrderBean.class, values, "dayno=?", dayno);
        Intent intent = new Intent(getApplicationContext(),
                PayresultActivity.class);
        intent.putExtra("intentclass", Constant.INTENT_CLASS_TAKE);
        startActivity(intent);
        finish();
    }

    /**
     * 确认收货
     */
    private void receipt() {
        if (orderid == null) {
            return;
        }
        Call<String> call = HttpManage.getRequestApi().
                updateOrder(SPManage.getInstance().getToken(), orderid, order_state);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String base) {
                Intent intent = new Intent(getApplicationContext(),
                        PayresultActivity.class);
                intent.putExtra("intentclass", Constant.INTENT_CLASS_TAKE);
                startActivity(intent);
                finish();
            }

            @Override
            public void onError(int code, String error) {
                Mtoast(error);
            }
        });

//        Call<BaseBean<String>> call= HttpManage.getRequestApi().
//                updateOrder(SPManage.getInstance().getToken(),orderid,order_state);
//        call.enqueue(new MyCallback<BaseBean<String>>() {
//            @Override
//            public void onSuccess(Call<BaseBean<String>> call,
//                                  Response<BaseBean<String>> response) {
//                Intent intent=new Intent(getApplicationContext(),
//                        PayresultActivity.class);
//                intent.putExtra("intentclass", Constant.INTENT_CLASS_TAKE);
//                startActivity(intent);
//                finish();
//            }
//
//            @Override
//            public void onError(int code, String error) {
//                 Mtoast(error);
//            }
//        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SunmiSdkManage.getInstance().showWelcomeImg();
    }
}
