package com.kinon.ceshiaccount.network;


import android.util.Log;

import com.kinon.ceshiaccount.network.http.HttpApiUrl;
import com.kinon.ceshiaccount.network.http.RequestApi;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by luohao on 2017-07-19.
 */

public class HttpManage {

    private static final String TAG = "lh_http";
    private static final int TIME_OUT = 10; //超时参数
    private static final int TIME_PAY_OUT = 3;  //支付超时时间
//    private static Retrofit retrofit=null;
    /**
     * 正常网络请求
     */
    private static RequestApi requestApi = null;
    /**
     * 需要耗时的网络请求
     */
    private static RequestApi longRequestApi = null;
    /**
     * 获取服务器地址的网络请求
     */
    private static RequestApi getUrlApi = null;
    private static RequestApi getLunchApi = null;
    /**
     * 上传时的超时时间
     */
    private static int SET_TIME_OUT = 3 * 60;

    private static OkHttpClient setApibuilder() {
        return setApibuilder(TIME_OUT);
    }

    /**
     * OKhttpclient的是设置
     */
    private static OkHttpClient setApibuilder(int time_out) {

        OkHttpClient okBuilder = new OkHttpClient().newBuilder()
                .addInterceptor(new LogInterceptor())
                //为构建者填充超时时间
                .connectTimeout(time_out, TimeUnit.SECONDS)
                .readTimeout(time_out, TimeUnit.SECONDS)
                .writeTimeout(time_out, TimeUnit.SECONDS)
                //允许逆向
                .followRedirects(true)
                .build();
        return okBuilder;
    }

    /**
     * 设置retrofit
     */
    private static Retrofit setRetrofit(String serverUrl, OkHttpClient client) {
        Log.w("WZY", serverUrl);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverUrl)
                //增加返回值为string的支持
                .addConverterFactory(ScalarsConverterFactory.create())
                //增加返回值为Gson的支持(以实体类返回)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }

    public static Retrofit setDownRetrofit(String url) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                //增加返回值为string的支持
                .addConverterFactory(ScalarsConverterFactory.create())
                //增加返回值为Gson的支持(以实体类返回)
                .addConverterFactory(GsonConverterFactory.create())
                .client(setApibuilder())
                .build();
        return retrofit;
    }

    /**
     * 正常网络请求的
     */
    private static void getRetrofit() {
    }

    private static void getApi() {
        requestApi = setRetrofit(HttpApiUrl.serverUrl, setApibuilder())
                .create(RequestApi.class);
    }

    public static RequestApi getRequestApi() {
        if (requestApi == null) {
            getApi();
        }
        return requestApi;
    }

    /**
     * 需要耗时的网络强请设置
     */
    private static void getLogApi() {
        longRequestApi = setRetrofit(HttpApiUrl.serverUrl, setApibuilder(SET_TIME_OUT))
                .create(RequestApi.class);
    }

    public static RequestApi getRequestApi(int time) {
        if (longRequestApi == null) {
            getLogApi();
        }
        return longRequestApi;
    }

    /**
     * 午餐支付的网络强请设置
     */
    private static void getLunchApi() {
        getLunchApi = setRetrofit(HttpApiUrl.serverUrl, setApibuilder(TIME_PAY_OUT))
                .create(RequestApi.class);
    }

    public static RequestApi getReqLunchApi() {
        if (getLunchApi == null) {
            getLunchApi();
        }
        return getLunchApi;
    }

    /**
     * 登录前获得地址的设置
     */
    private static void getUrlApi() {
        getUrlApi = setRetrofit(HttpApiUrl.serverGetUrl, setApibuilder())
                .create(RequestApi.class);
    }

    public static RequestApi getRequestUrlApi() {
        if (getUrlApi == null) {
            getUrlApi();
        }
        return getUrlApi;
    }

    // OkHttp3 的监听
    private static class LogInterceptor implements Interceptor {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Log.i(TAG, "okhttp3:" + request.toString());//输出请求前整个url
            long t1 = System.nanoTime();
            okhttp3.Response response = chain.proceed(chain.request());
            long t2 = System.nanoTime();
//			Log.v(TAG,response.request().url()+response.headers());//输出一个请求的网络信息
            okhttp3.MediaType mediaType = response.body().contentType();
            String content = response.body().string();
            Log.i(TAG, "response body:" + content);//输出返回信息
            return response.newBuilder()
                    .body(okhttp3.ResponseBody.create(mediaType, content))
                    .build();
        }
    }

}
