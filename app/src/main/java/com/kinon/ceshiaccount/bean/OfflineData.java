package com.kinon.ceshiaccount.bean;

import java.util.List;

/**
 * 佛祖保佑 永无BUG
 *
 * @author WangZhiYao
 * @date 2018/5/4
 */
public class OfflineData {

    private List<String> ordernos;

    public List<String> getOrdernos() {
        return ordernos;
    }

    public void setOrdernos(List<String> ordernos) {
        this.ordernos = ordernos;
    }

    @Override
    public String toString() {
        return "OfflineData{" +
                "ordernos=" + ordernos +
                '}';
    }
}
