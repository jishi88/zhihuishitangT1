package com.kinon.ceshiaccount.adapter.urgent;

import android.content.ContentValues;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.bean.urgent.U_SettletmentBean;
import com.kinon.ceshiaccount.view.CustomDialog;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;

import static com.kinon.ceshiaccount.R.id.tv_orderRefunds;

/**
 * Created by luohao on 2017-07-19.
 * 记录中的订单适配器
 */

public class U_RecordAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<U_SettletmentBean> list;

    public U_RecordAdapter(Context context, ArrayList<U_SettletmentBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
//        Log.e("lh","getView---");
        final ViewHandler handler;
        if (view == null) {
            handler = new ViewHandler();
            view = LayoutInflater.from(context).inflate(R.layout.item_record_layout, null);
            handler.tv_orderId = (TextView) view.findViewById(R.id.tv_orderId);
            handler.tv_orderName = (TextView) view.findViewById(R.id.tv_orderName);
            handler.tv_orderMoney = (TextView) view.findViewById(R.id.tv_orderMoney);
            handler.tv_orderTime = (TextView) view.findViewById(R.id.tv_orderTime);
            handler.tv_orderRefunds = (TextView) view.findViewById(tv_orderRefunds);
            view.setTag(handler);
        } else {
            handler = (ViewHandler) view.getTag();
        }
        handler.tv_orderId.setText(list.get(i).getOrderno());
        handler.tv_orderName.setText(list.get(i).getCard());
        handler.tv_orderMoney.setText(list.get(i).getTotal());
        handler.tv_orderTime.setText(list.get(i).getTime());

        int refund = list.get(i).getRefund();
        if (refund == 0) {
            handler.tv_orderRefunds.setText(R.string.request_refund);
            handler.tv_orderRefunds.setClickable(true);
        } else if (refund == 1) {
            handler.tv_orderRefunds.setText(R.string.refund_processing);
            handler.tv_orderRefunds.setClickable(false);
        } else {
            handler.tv_orderRefunds.setText(R.string.refund_complete);
            handler.tv_orderRefunds.setClickable(false);
        }
        handler.tv_orderRefunds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog(handler.tv_orderRefunds, i);
            }
        });
        return view;
    }

    private void dialog(final TextView tv_orderRefunds, final int position) {
        CustomDialog dialog = new CustomDialog(context, null, "是否申请退款", null, null,
                new CustomDialog.DialogClickListener() {
                    @Override
                    public void onDialogClick(int btn) {
                        if (btn == 1) {
                            refund(position);
                        }
                    }
                });
        dialog.show();
    }

    private void refund(int position) {
        String id = list.get(position).getOrderno();
        list.get(position).setRefund(1);
        ContentValues values = new ContentValues();
        values.put("refund", 1);
        DataSupport.updateAll(U_SettletmentBean.class, values, "orderno=?", id);
        notifyDataSetChanged();
    }

    private class ViewHandler {
        private TextView tv_orderId;
        private TextView tv_orderName;
        private TextView tv_orderMoney;
        private TextView tv_orderTime;
        private TextView tv_orderRefunds;
    }


}
