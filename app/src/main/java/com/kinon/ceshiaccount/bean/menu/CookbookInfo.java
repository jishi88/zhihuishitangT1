package com.kinon.ceshiaccount.bean.menu;

import java.util.ArrayList;

/**
 * Created by lhqq on 2017-12-08.
 */

public class CookbookInfo {
    //    private String date;
    private ArrayList<Breakfast> breakfast;
    private ArrayList<Breakfast> lunch;
    private ArrayList<Breakfast> dinner;

//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }

    public ArrayList<Breakfast> getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(ArrayList<Breakfast> breakfast) {
        this.breakfast = breakfast;
    }

    public ArrayList<Breakfast> getLunch() {
        return lunch;
    }

    public void setLunch(ArrayList<Breakfast> lunch) {
        this.lunch = lunch;
    }

    public ArrayList<Breakfast> getDinner() {
        return dinner;
    }

    public void setDinner(ArrayList<Breakfast> dinner) {
        this.dinner = dinner;
    }
}
