package com.kinon.ceshiaccount.bean;

/**
 * Created by lhqq on 2017-12-21.
 */

public class TotalTypeBean {

    private String name;
    private String total;
    private String unit;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
