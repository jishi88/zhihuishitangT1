package com.kinon.ceshiaccount.util;

import com.kinon.ceshiaccount.bean.RoleBean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-09-08.
 */

public class ShowDataManager {

    private static ShowDataManager dataManager = null;
    private ArrayList<RoleBean> role;

    public static ShowDataManager getInstance() {
        if (dataManager == null) {
            synchronized (ShowDataManager.class) {
                dataManager = new ShowDataManager();
            }
        }
        return dataManager;
    }

    public ArrayList<RoleBean> getRole() {
        return role;
    }

    public void setRole(ArrayList<RoleBean> role) {
        this.role = role;
    }
}
