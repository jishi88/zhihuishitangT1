package com.kinon.ceshiaccount.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.kinon.ceshiaccount.application.AccountApplication;

/**
 * Created by luohao on 2017-08-14.
 */

public class LoadDialogManager {
    private static LoadDialogManager mInstnce = null;
    private ProgressDialog mDialog = null;

    public static LoadDialogManager gerInstnce() {

        if (mInstnce == null) {
            synchronized (LoadDialogManager.class) {
                mInstnce = new LoadDialogManager();
            }
        }

        return mInstnce;
    }

    public void showProgressDialog(Context context, String msg) {
        if (mDialog == null) {
            mDialog = new ProgressDialog(context);
            mDialog.setMessage(msg);
            mDialog.setCanceledOnTouchOutside(false);
//            mDialog.setIndeterminate();

        }

        mDialog.show();
        DisplayMetrics metrics = getMetrics(context);
        WindowManager.LayoutParams dialogParams = mDialog.getWindow().getAttributes();
        dialogParams.width = 600 /*(int) (metrics.widthPixels*0.85)*/;
        dialogParams.height = 200 /* (int) (metrics.widthPixels*0.85)*/;
        mDialog.getWindow().setAttributes(dialogParams);
//        mDialog.setContentView(R.layout.custon_progressdialog_layout);

    }

    public DisplayMetrics getMetrics(Context context) {
        WindowManager windowManager = (WindowManager)
                AccountApplication.getInstance().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
//        DisplayMetrics displaysMetrics = new DisplayMetrics();
//        context.getWindowManager().getDefaultDisplay().getMetrics(displaysMetrics);
        return dm;
    }

//    private void setwindow(ProgressDialog dialog,Context con){
//        Window window = dialog.getWindow() ;
//        WindowManager m = con.getWindowManager();
//        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高用
//        WindowManager.LayoutParams p = window.getAttributes(); // 获取对话框当前的参数值
//        p.height = (int) (d.getHeight() * 0.6); // 改变的是dialog框在屏幕中的位置而不是大小
//        p.width = (int) (d.getWidth() * 0.65); // 宽度设置为屏幕的0.65
//        window.setAttributes(p);
//    }


    public void dismissProgressDialog() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
        mDialog = null;
    }
}
