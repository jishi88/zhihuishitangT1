package com.kinon.ceshiaccount.fragment.home;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.fragment.BaseFragment;
import com.kinon.ceshiaccount.network.HttpManage;
import com.kinon.ceshiaccount.network.StrCallback;
import com.kinon.ceshiaccount.util.SPManage;
import com.kinon.ceshiaccount.util.ToastUtil;
import com.kinon.ceshiaccount.view.computer.ComputerNumView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2018-03-01.
 */

public class RechargeFragment extends BaseFragment implements View.OnClickListener,
        ComputerNumView.OnComputerListener, View.OnFocusChangeListener {
    ToastUtil toastUtil = new ToastUtil();
    private View mView;
    private EditText et_rechargeCard;
    private EditText et_rechargeMoney;
    private Button btn_recharge;
    private ComputerNumView cview_numRecharge;
    private String strMoney = "";
    private int mEtType = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_recharge_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        et_rechargeCard = mView.findViewById(R.id.et_rechargeCard);
        et_rechargeMoney = mView.findViewById(R.id.et_rechargeMoney);
        btn_recharge = mView.findViewById(R.id.btn_recharge);
        cview_numRecharge = mView.findViewById(R.id.cview_numRecharge);
        addClick();
    }

    private void addClick() {
        et_rechargeCard.setOnFocusChangeListener(this);
        et_rechargeMoney.setOnFocusChangeListener(this);
        btn_recharge.setOnClickListener(this);
        cview_numRecharge.setOnComputerListener(this);
        hidekey(et_rechargeCard);
        hidekey(et_rechargeMoney);
    }

    //不弹出键盘
    private void hidekey(EditText et) {
        getActivity().getWindow().setSoftInputMode
                (WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        try {
            Class<EditText> cls = EditText.class;
            Method setSoftInputShownOnFocus;
            setSoftInputShownOnFocus = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
            setSoftInputShownOnFocus.setAccessible(true);
            setSoftInputShownOnFocus.invoke(et, false);
        } catch (Exception e) {

        }
    }

    private void recharge() {
        String card = et_rechargeCard.getText().toString().trim();
        if (TextUtils.isEmpty(card)) {
            toastUtil.Short("请扫卡")
                    .setToastBackground(Color.WHITE, 0).show();
            return;
        }
        if (TextUtils.isEmpty(strMoney)) {
            toastUtil.Short("请输入金额")
                    .setToastBackground(Color.WHITE, 0).show();
            return;
        }
        Call<String> call = HttpManage.getReqLunchApi().recharge(
                SPManage.getInstance().getToken(),
                card, strMoney);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) throws JSONException {
                JSONObject obje = new JSONObject(response.body());
                toastUtil.Short(obje.getString("msg"))
                        .setToastBackground(Color.WHITE, 0).show();
                clearInfo();
            }

            @Override
            public void onError(int code, String error) {
                toastUtil.Short(error)
                        .setToastBackground(Color.WHITE, 0).show();
            }
        });

    }

    private void clearInfo() {
        et_rechargeCard.setText("");
        strMoney = "";
        et_rechargeMoney.setText(strMoney);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_recharge:
                recharge();
                break;
            default:
                break;
        }
    }

    @Override
    public void onComputerListener(String strNum) {
        //p代表.
        if (strNum.equals("p")) {
            //小数点
            counPoint();
            return;
        }
        //j代表加
        else if (strNum.equals("j")) {
            //加号计算
            return;
        }
        //c代表清除
        else if (strNum.equals("c")) {
            //清除计算
            switch (mEtType) {
                case 0:
                    et_rechargeCard.setText("");
                    break;
                case 1:
                    strMoney = "";
                    et_rechargeMoney.setText(strMoney);
                    break;


            }
        }
        //o代表完成
        else if (strNum.equals("o")) {

        }
        //d代表删除上一个
        else if (strNum.equals("d")) {
            //删除上一个

            switch (mEtType) {
                case 0:
                    et_rechargeCard.setText("");
                    break;
                case 1:
                    if (strMoney.length() > 0) {
                        strMoney = strMoney.substring(0, strMoney.length() - 1);
                        et_rechargeMoney.setText(strMoney);
                    }
                    break;
            }

        }
        //除了上面几种特殊键就是数字键
        else {
            switch (mEtType) {
                case 0:
                    break;
                case 1:
                    strMoney += strNum;
                    et_rechargeMoney.setText(strMoney);
                    break;

            }

        }
    }

    /**
     * 小数点的计算
     */
    private void counPoint() {
        if (mEtType != 0) {
            //判断etNum中是否已经存在.
            if (strMoney.contains(".")) {
                return;
            } else {
                strMoney += ".";
                et_rechargeMoney.setText(strMoney);
            }
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.et_rechargeCard:
                if (b) {
                    mEtType = 0;
                }
                break;
            case R.id.et_rechargeMoney:
                if (b) {
                    mEtType = 1;
                }
                break;

        }
    }
}
