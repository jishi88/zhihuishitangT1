package com.kinon.ceshiaccount.bean;

import java.io.Serializable;

/**
 * Created by luohao on 2017-08-22.
 * 商品的详情
 */

public class DetailsBean implements Serializable {
    private int did;
    private int amount;
    private double price;
    private double total;
    private int id;
    private String name;
    private String image;
    private String unit;
    private String content;
    private int stock;
    private int perlim;
    private int daylim;

    public DetailsBean(int did, int amount, double price, double total, int id,
                       String name, String image, String unit, String content,
                       int stock, int perlim, int daylim) {
        this.did = did;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.id = id;
        this.name = name;
        this.image = image;
        this.unit = unit;
        this.content = content;
        this.stock = stock;
        this.perlim = perlim;
        this.daylim = daylim;
    }

    public int getDid() {
        return did;
    }

    public void setDid(int did) {
        this.did = did;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPerlim() {
        return perlim;
    }

    public void setPerlim(int perlim) {
        this.perlim = perlim;
    }

    public int getDaylim() {
        return daylim;
    }

    public void setDaylim(int daylim) {
        this.daylim = daylim;
    }
}
