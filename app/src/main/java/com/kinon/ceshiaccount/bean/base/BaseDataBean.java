package com.kinon.ceshiaccount.bean.base;

/**
 * Created by luohao on 2017-07-19.
 * 包含data的实体类
 */

public class BaseDataBean {
    private boolean code;
    private String msg;
    private String data;

    public boolean isCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
