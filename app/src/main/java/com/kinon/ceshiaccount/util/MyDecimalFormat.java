package com.kinon.ceshiaccount.util;

import java.text.DecimalFormat;

/**
 * Created by luohao on 2017-07-18.
 * 用来初始化double数据保留小数点后两位
 */

public class MyDecimalFormat {

    //    private static MyDecimalFormat mInstance=null;
    private static DecimalFormat mDformat = null;

    public static String dFormat(double dou) {
        if (mDformat == null) {
            mDformat = new DecimalFormat("#.00");
        }
        return mDformat.format(dou);
    }

}
