package com.kinon.ceshiaccount.util;

/**
 * 佛祖保佑 永无BUG
 *
 * @author WangZhiYao
 * @date 2018/4/10
 */
public class RegexUtils {

    private static final String REGEX_NUMBER = "^[0-9]*$";

    public static boolean isNumber(String s) {
        return !s.isEmpty() && s.matches(REGEX_NUMBER);
    }
}
