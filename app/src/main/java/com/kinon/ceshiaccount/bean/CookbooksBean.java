package com.kinon.ceshiaccount.bean;

import com.kinon.ceshiaccount.bean.menu.CookbookInfo;

import java.util.ArrayList;

/**
 * Created by lhqq on 2017-12-08.
 */

public class CookbooksBean {
    private ArrayList<Category> category;
    private CookbookInfo cookbooks;


    public CookbookInfo getCookbooks() {
        return cookbooks;
    }

    public void setCookbooks(CookbookInfo cookbooks) {
        this.cookbooks = cookbooks;
    }

    public ArrayList<Category> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<Category> category) {
        this.category = category;
    }

    /**
     * 餐单分类
     */
    public class Category {
        private int id;
        private String name;
        private String description;
        private int position;
        private int lim;
        private int category_type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public int getLim() {
            return lim;
        }

        public void setLim(int lim) {
            this.lim = lim;
        }

        public int getCategory_type() {
            return category_type;
        }

        public void setCategory_type(int category_type) {
            this.category_type = category_type;
        }
    }
}


