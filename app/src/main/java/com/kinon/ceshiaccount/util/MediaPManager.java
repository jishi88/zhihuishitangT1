package com.kinon.ceshiaccount.util;

import android.media.MediaPlayer;

import com.kinon.ceshiaccount.R;
import com.kinon.ceshiaccount.application.AccountApplication;

/**
 * Created by luohao on 2017-10-26.
 */

public class MediaPManager {
    private static MediaPManager mMediaPManager = null;
    private static MediaPlayer mMediapS = null;
    private static MediaPlayer mMediapF = null;


    private MediaPManager() {
        mMediapS = MediaPlayer.create(AccountApplication.getInstance(), R.raw.pay_success);
        mMediapF = MediaPlayer.create(AccountApplication.getInstance(), R.raw.pay_failure);
    }

    public static MediaPManager getInstance() {
        synchronized (mMediaPManager) {
            if (mMediaPManager == null || mMediapS == null || mMediapF == null) {
                mMediaPManager = new MediaPManager();
            }
        }
        return mMediaPManager;
    }

    public void mediaPlay(int media) {
        if (media == 1) {
            mMediapS.start();
        } else {
            mMediapF.start();
        }

    }

}
