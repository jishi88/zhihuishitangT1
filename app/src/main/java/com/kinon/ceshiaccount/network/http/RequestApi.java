package com.kinon.ceshiaccount.network.http;

import com.google.gson.JsonObject;
import com.kinon.ceshiaccount.bean.OfflineData;
import com.kinon.ceshiaccount.bean.UrlBean;
import com.kinon.ceshiaccount.bean.base.BaseBean;
import com.kinon.ceshiaccount.bean.urgent.Card;
import com.kinon.ceshiaccount.bean.urgent.U_OrderBean;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by luohao on 2017-07-19.
 */

public interface RequestApi {

    /**
     * 获得后面请求的地址
     *
     * @param sn 机器的序列号 SN
     * @return
     */
    @FormUrlEncoded
    @POST("company/get_by_machine")
    Call<BaseBean<UrlBean>> getByMachine(@Field("machine_code") String sn);

    /**
     * 获得订单列表
     *
     * @param token
     * @param page
     * @param offset
     * @param type
     * @return
     */
    @FormUrlEncoded
    @POST("order/getOrders")
    Call<String> getOrders(@Field("token") String token,
                           @Field("page") String page,
                           @Field("offset") String offset,
                           @Field("type") String type);


    /**
     * 退款
     */
    @FormUrlEncoded
    @POST("order/refundDeal")
    Call<String> refund(@Field("token") String token,
                        @Field("orderid") String orderid);


    /**
     * 登录
     *
     * @param username
     * @param password
     * @param type     登录的用户类型
     * @return
     */
    @FormUrlEncoded
    @POST("passport/login_app")
    Call<String> login(@Field("username") String username,
                       @Field("password") String password,
                       @Field("type") String type,
                       @Field("offline") int offline,
                       @Field("appversion") String appversion,
                       @Field("code") String code);

    @FormUrlEncoded
    @POST("passport/login_app")
    Call<JsonObject> login2(@Field("username") String username,
                            @Field("password") String password,
                            @Field("type") String type);

    /**
     * 午餐支付
     *
     * @param token
     * @param no
     * @param type
     * @param stamp
     * @param access 登录时返回的参数
     * @return
     */
    @FormUrlEncoded
    @POST("order/lunchPay")
    Call<String> lunchPay(@Field("token") String token,
                          @Field("no") String no,
                          @Field("type") String type,
                          @Field("stamp") String stamp,
                          @Field("access") int access);

    /**
     * 通知
     */
    @FormUrlEncoded
    @POST("order/noticeUser")
    Call<BaseBean<String>> noticeUser(@Field("token") String token,
                                      @Field("orderid") String orderid);

    /**
     * 无商品保存订单
     */
    @FormUrlEncoded
    @POST("order/saveNewOrder_v2")
    Call<BaseBean<String>> saveNewOrder(@Field("token") String token,
                                        @Field("total") String total,
                                        @Field("type") String type,
                                        @Field("stamp") String stamp);

    /**
     * 结算支付
     */
    @FormUrlEncoded
    @POST("order/otherPay")
    Call<String> otherPay(@Field("token") String token,
                          @Field("orderid") String orderid,
                          @Field("no") String no,
                          @Field("stamp") String stamp,
                          @Field("access") int access/*,
                           @Field("type") String type*/);

    /**
     * 新结算支付
     */
    @FormUrlEncoded
    @POST("order/otherPay_v2")
    Call<String> otherPay_v2(@Field("token") String token,
                             @Field("total") String total,
                             @Field("no") String no,
                             @Field("stamp") String stamp,
                             @Field("access") int access,
                             @Field("type") String type);

    /**
     * 取消结算订单
     */
    @FormUrlEncoded
    @POST("order/cancelOrder")
    Call<BaseBean<String>> cancelOrder(@Field("token") String token,
                                       @Field("orderid") String orderid);

    /**
     * 获得取餐的商品信息
     */
    @FormUrlEncoded
    @POST("order/getDetail")
    Call<String> getDetail(@Field("token") String token,
                           @Field("orderno") String orderno,
                           @Field("dayno") String dayno,
                           @Field("orderid") String orderid);

    /**
     * 确认收货
     */
    @FormUrlEncoded
    @POST("order/updateOrder")
    Call<String> updateOrder(@Field("token") String token,
                             @Field("orderid") String orderid,
                             @Field("state") String state);

    /**
     * 打印
     */
    @FormUrlEncoded
    @POST("order/printOrder")
    Call<String> printOrder(@Field("token") String token,
                            @Field("orderid") String orderid);


    /**
     * 一键通知
     */
    @FormUrlEncoded
    @POST("order/noticeAllUsers")
    Call<String> noticeAllUsers(@Field("token") String token);

    /**
     * 获取工作人员的信息
     */
    @FormUrlEncoded
    @POST("admin/get_admin_by_token")
    Call<String> getAdmin(@Field("token") String token);


    /**
     * 获取紧急情况下的订单记录
     *
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("backup/getOrders")
    Call<BaseBean<ArrayList<U_OrderBean>>> backupGetOrders(@Field("token") String token);


    /**
     * 获取所有卡号
     *
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("backup/getCodes")
    Call<BaseBean<ArrayList<Card>>> backupGetCards(@Field("token") String token);


    /**
     * 提交刷卡记录
     */
    @FormUrlEncoded
    @POST("backup/dealOrder")
    Call<String> backupdealOrder(@Field("token") String token,
                                 @Field("data") String data,
                                 @Field("status") int status);

    /**
     * 提交取餐记录
     */
    @FormUrlEncoded
    @POST("backup/dealSorder")
    Call<String> backupdealSorder(@Field("token") String token,
                                  @Field("data") String data,
                                  @Field("status") int status);

    /**
     * 绑卡获取验证码
     */
    @FormUrlEncoded
    @POST("order/sendCode")
    Call<BaseBean<String>> sendCode(@Field("token") String token,
                                    @Field("phone") String phone);


    /**
     * 新用户绑卡获取验证码
     *
     * @param token
     * @param phone
     * @return
     */
    @FormUrlEncoded
    @POST("order/sendCode_v2")
    Call<BaseBean<String>> sendCodeV2(@Field("token") String token,
                                      @Field("phone") String phone);

    /**
     * 绑卡
     */
    @FormUrlEncoded
    @POST("order/bindCard")
    Call<BaseBean<String>> bindCard(@Field("token") String token,
                                    @Field("phone") String phone,
                                    @Field("no") String no,
                                    @Field("code") String code);

    /**
     * 新用户绑卡
     *
     * @param token
     * @param phone
     * @param no
     * @param code
     * @return
     */
    @FormUrlEncoded
    @POST("order/bindCard_v2")
    Call<BaseBean<String>> bindCardV2(@Field("token") String token,
                                      @Field("phone") String phone,
                                      @Field("no") String no,
                                      @Field("code") String code);

    /**
     * 绑指纹
     */
    @FormUrlEncoded
    @POST("order/bindFinger")
    Call<String> bindFinger(@Field("token") String token,
                            @Field("no") String no,
                            @Field("finger") String finger);

    /**
     * 刷卡取餐
     */
    @FormUrlEncoded
    @POST("order/takeOrder")
    Call<String> cardTake(@Field("token") String token,
                          @Field("no") String no);


    /**
     * 刷卡取餐
     */
    @FormUrlEncoded
    @POST("order/getUser")
    Call<String> phoneCheck(@Field("token") String token,
                            @Field("phone") String phone);


    /**
     * 图片下载
     */
    @Streaming
    @GET
    Call<ResponseBody> downloadImg(@Url String imgUrl);

    /**
     * 获取菜单
     */
    @FormUrlEncoded
    @POST("admin/getCookbooks")
    Call<String> getCookbooks(@Field("token") String token);

    /**
     * 获取商品
     *
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("admin/getNewCookbooks")
    Call<String> getNewCookbooks(@Field("token") String token);


    /**
     * 提交菜单
     *
     * @param token
     * @param type
     * @param total
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("admin/saveProductOrder")
    Call<BaseBean<String>> saveProductOrder(@Field("token") String token,
                                            @Field("type") String type,
                                            @Field("total") String total,
                                            @Field("data") String data,
                                            @Field("stamp") String stamp);

    /**
     * 获取金额总计
     *
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("admin/getData")
    Call<String> getTotalData(@Field("token") String token);


    /**
     * 获取用户余额和消费信息
     *
     * @param token
     * @param no
     * @param page
     * @param offset
     * @return
     */
    @FormUrlEncoded
    @POST("admin/getUserByCode")
    Call<String> getUserByCode(@Field("token") String token,
                               @Field("no") String no,
                               @Field("page") int page,
                               @Field("offset") int offset);

    /**
     * 充值
     *
     * @param token
     * @param no
     * @param money
     * @return
     */
    @FormUrlEncoded
    @POST("admin/recharge")
    Call<String> recharge(@Field("token") String token,
                          @Field("no") String no,
                          @Field("money") String money);


    /**
     * 获取下载地址
     *
     * @param sn
     * @return
     */
    @FormUrlEncoded
    @POST("home/getURL")
    Call<String> getDownLoadUrl(@Field("code") String sn);


    /**
     * 下载apk @Streaming声明是为了使文件不加载进内存
     *
     * @param url
     * @return
     */
    @Streaming
    @GET
    Call<ResponseBody> downLoadApk(@Url String url);

    @FormUrlEncoded
    @POST("backup/dealOrder_v2")
    Call<BaseBean<OfflineData>> uploadOfflineData(@Field("token") String token,
                                                  @Field("data") String data,
                                                  @Field("status") int status);
}
