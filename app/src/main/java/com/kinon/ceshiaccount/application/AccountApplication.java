package com.kinon.ceshiaccount.application;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.kinon.ceshiaccount.sunmi.print.AidlUtil;

import org.litepal.LitePal;

/**
 * Created by luohao on 2017-06-15.
 */

public class AccountApplication extends Application {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    public static String serialNum;
    public static String subModel;

    public static int SCREEN_WIDTH = -1;
    public static int SCREEN_HEIGHT = -1;
    //分辨率
    public static float DIMEN_RATE = -1.0F;
    //屏幕密度
    public static int DIMEN_DPI = -1;
    private static AccountApplication mApplication = null;
    private static String[] PERMISSIONS_STORAGE = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE"};

    public static AccountApplication getInstance() {

        return mApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        serialNum = android.os.Build.SERIAL;
        mApplication = this;
        getScreenSize();
        //连接打印
        AidlUtil.getInstance().connectPrinterService(this);
        //加载数据库
        LitePal.initialize(this);
        getStrSubMode();
    }

    private void getStrSubMode() {
        subModel = Settings.Global.getString(getContentResolver(), "sunmi_sub_model");
        Log.e("lh", "getStrSubMode====" + subModel);
    }


    public void getScreenSize() {
        WindowManager windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        Display display = windowManager.getDefaultDisplay();
        display.getMetrics(dm);
        DIMEN_RATE = dm.density / 1.0F;
        DIMEN_DPI = dm.densityDpi;
        SCREEN_WIDTH = dm.widthPixels;
        SCREEN_HEIGHT = dm.heightPixels;
        if (SCREEN_WIDTH > SCREEN_HEIGHT) {
            int t = SCREEN_HEIGHT;
            SCREEN_HEIGHT = SCREEN_WIDTH;
            SCREEN_WIDTH = t;
        }
    }


}
