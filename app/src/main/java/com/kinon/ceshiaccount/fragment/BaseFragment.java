package com.kinon.ceshiaccount.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.kinon.ceshiaccount.application.AccountApplication;

/**
 * Created by luohao on 2017-06-15.
 */

public class BaseFragment extends Fragment {
    protected static String TAG = "lh";
    protected Context mContext = AccountApplication.getInstance();


    public void Mtoast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


}
