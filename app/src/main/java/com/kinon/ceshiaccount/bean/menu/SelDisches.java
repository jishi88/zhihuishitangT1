package com.kinon.ceshiaccount.bean.menu;

/**
 * Created by lhqq on 2017-12-08.
 */

public class SelDisches {

    private int id;
    private int productid;
    private String name;
    private double price;
    private int amount;
    private int index;

    public SelDisches(int productid, String name, double price,
                      int amount, int index) {
        this.productid = productid;
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.index = index;
    }

    public SelDisches(int id, int productid, String name, double price, int amount, int index) {
        this.id = id;
        this.productid = productid;
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.index = index;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getNum() {
        return amount;
    }

    public void setNum(int num) {
        this.amount = num;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
