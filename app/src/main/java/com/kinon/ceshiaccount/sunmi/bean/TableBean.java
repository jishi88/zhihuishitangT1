package com.kinon.ceshiaccount.sunmi.bean;

/**
 * Created by Administrator on 2017/5/24.
 */

public class TableBean {
    private String[] text;

    private int[] align;

    public TableBean(String[] text) {
        this.text = text;
    }

    public TableBean(String[] text, int[] align) {
        this.text = text;

        this.align = align;
    }

    public String[] getText() {
        return text;
    }

    public void setText(String[] text) {
        this.text = text;
    }

    public int[] getAlign() {
        return align;
    }

    public void setAlign(int[] align) {
        this.align = align;
    }
}
